﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Models
{
    public class UserGroupLine
    {  
        public int GroupId { get; set; }
        public string UserId { get; set; }
        [ForeignKey("GroupId")]
        public virtual  UserGroup Group { get; set; }       
        [ForeignKey("UserId")]
        public virtual  ApplicationUser User { get; set; }
    }
}
