﻿using Data.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data
{
    public class Exam
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string Title { get; set; }
        [Required]
        [StringLength(200)]
        public string Description { get; set; }
        [Required]
        public string CreatedById { get; set; }
        public int SubjectId {get; set; }
        public DateTime FromDate { get; set; }
        public int ToDate { get; set; }
        
        [ForeignKey("StatusId")]
        public virtual ExamStatus Status { get; set; }
        public int EvaluateType { get; set; }

        [ForeignKey("CreatedById")]
        public virtual  ApplicationUser Createdby { get; set; }
        [ForeignKey("SubjectId")]
        public virtual  Subject Subject { get; set; }

        public virtual ICollection<ExamLine> ExamLines { get; set; }
        public virtual ICollection<UserGroupToExam>UserGroupToExam { get; set; }
    }
}
