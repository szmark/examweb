﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Models
{
    public class ExamLine
    {       
        public int QuestionId { get; set; }
        public int ExamId { get; set; }
        public string UserId { get; set; }       

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LineId { get; set; }

        [ForeignKey("QuestionId")]
        public virtual Question Question { get; set; }        
        [ForeignKey("ExamId")]
        public virtual Exam Exam { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        [Required]
        [StringLength(500)]
        public string QuestionName { get; set; }
        public double? Value { get; set; }
    }
}
