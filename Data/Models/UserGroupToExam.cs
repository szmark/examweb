﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Models
{
    public class UserGroupToExam
    { 
        public int GroupId { get; set; }
        public int ExamId { get; set; }
        [ForeignKey("ExamId")]
        public virtual Exam Exam { get; set; }        
        [ForeignKey("GroupId")]
        public virtual UserGroup UserGroup { get; set; }
    }
}
