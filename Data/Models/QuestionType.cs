﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models
{
    public enum QuestionType
    {
        Option,
        Text,
        TrueOrFalse
    }
}
