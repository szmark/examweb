﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Models
{
    public class Question
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [StringLength(500)]
        public string Name { get; set; }
        [ForeignKey("SubjectId")]
        public virtual  Subject Subject { get; set; }
        public virtual ICollection<ExamLine> ExamLines { get; set; }      
        public virtual  ICollection<Answer> Answers { get; set; }

    }
}
