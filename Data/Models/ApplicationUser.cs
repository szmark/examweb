﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Data.Models
{
    public class ApplicationUser:IdentityUser
    {
        public virtual  ICollection<UserGroupLine> UserGroupLines { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
    }
}
