﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Models
{
    public class Answer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int  Id { get; set; }
        [StringLength(500)]
        public string TextAnswer { get; set; }
        public bool TrueOrFalse { get; set; }
        [StringLength(500)]
        public string AnswerA { get; set; }
        [StringLength(500)]
        public string AnswerB { get; set; }
        public bool IsCorrect { get; set; }
        public int QuestionType { get; set;}
        public string UserId { get; set; }
        public int QuestionId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }        
        [ForeignKey("QuestionId")]
        [Required]
        public virtual Question Question { get; set; }
    }
}
