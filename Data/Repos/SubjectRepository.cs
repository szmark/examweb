﻿using Data.Interfaces;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repos
{
    public class SubjectRepository : GenericCrudRepository<Subject>, ISubjectRepository
    {
        public SubjectRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<Subject> GetById(int id)
        {
            return await _context.Subjects.FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
