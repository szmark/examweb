﻿using Data.Interfaces;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repos
{
    public class UserRepository : GenericCrudRepository<ApplicationUser>, IUserRepository
    {
        //protected ApplicationDbContext _context;
        public UserRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        
        public DatabaseFacade Database
        {
            get { return _context.Database; }
        }

        public async Task<ApplicationUser> FindByEmailAndPassword(string email,string password)
        {
            return await _context.Set<ApplicationUser>()
                .SingleOrDefaultAsync(x => x.Email == email && x.PasswordHash == password);                
        }

        public async Task<ApplicationUser> FindById(string id)
        {
            return await _context.Set<ApplicationUser>().FirstOrDefaultAsync(u => u.Id == id);
        }     
    }
}
