﻿using Data.Interfaces;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repos
{
    public class QuestionRepository : GenericCrudRepository<Question>, IQuestionRepository
    {
        public QuestionRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public DatabaseFacade Database
        {
            get { return _context.Database; }
        }

        public async Task<IQueryable<Question>> FindBySubject(Subject subject)
        {
            List<Question> list = await _context.Questions.Where(x => x.Subject.Id == subject.Id).ToListAsync();
            return list.AsQueryable();
        }

        public async Task<Question> GetById(int id)
        {
            return await _context.Questions.FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
