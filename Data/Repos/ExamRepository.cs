﻿using Data.Interfaces;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repos
{
    public class ExamRepository : GenericCrudRepository<Exam>, IExamRepository
    {       
        public ExamRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public DatabaseFacade Database
        {
            get { return _context.Database; }
        }

        public async Task<IQueryable<Exam>> FindByCreator(string creatorid)
        {
            List<Exam> list= await  _context.Exams.Where(x => x.CreatedById == creatorid)
                 //.Include(x => x.Status)
                //.Include(x => x.Subject)
                //.Include(x => x.UserGroupToExam)
                //.Include(x => x.ExamLines.Select(line=>line.Question).Select(q=>q.Answers))  
                    //.ThenInclude(y => y.AsQueryable().Include(line=>line.Question == null ? new Question() { } : line.Question).
                           // ThenInclude(q =>q==null ? new List<Answer>(): q.Answers))
                           .ToListAsync();
            return list.AsQueryable();
        }

        public async Task<Exam> FindById(int id)
        {
            return await _context.Set<Exam>().FirstOrDefaultAsync(u => u.Id == id);
        }        
    }
}
