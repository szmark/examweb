﻿using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repos
{
    public abstract class GenericCrudRepository<T> : IRepository<T> where T : class

    {
        protected ApplicationDbContext _context;

        /*public GenericCrudRepository(ApplicationDbContext context)
        {
            this._context = context;
        }*/
        public virtual async Task<int> Create(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            _context.Set<T>().Attach(entity);
            _context.Set<T>().Add(entity);
            return await _context.SaveChangesAsync();
        }

        public virtual async Task<int> Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            _context.Set<T>().Attach(entity);
            _context.Entry(entity).State = EntityState.Deleted;
            return await _context.SaveChangesAsync();
        }

        public virtual async Task<IReadOnlyCollection<T>> GetAll()
        {
            return await _context.Set<T>().ToListAsync();
        }

        public virtual async Task<int> Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            _context.Set<T>().Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            return await _context.SaveChangesAsync();
        }
    }
}
