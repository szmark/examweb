﻿using Data.Interfaces;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repos
{
    public class UserGroupLineRepository : GenericCrudRepository<UserGroupLine>, IUserGroupLineRepository
    {
        public UserGroupLineRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IQueryable<UserGroupLine>> FindByUser(string userid)
        {
            List<UserGroupLine> list = await _context.UserGroupLines.Where(x => x.UserId == userid).ToListAsync();
            return list.AsQueryable();
        }

        public async Task<UserGroupLine> GetByKey(int groupid, string userid)
        {
            return await _context.UserGroupLines.FirstOrDefaultAsync(x => x.GroupId== groupid && x.UserId == userid);
        }
    }
}
