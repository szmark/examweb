﻿using Data.Interfaces;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repos
{
    public class AnswerRepository : GenericCrudRepository<Answer>, IAnswerRepository
    {
        public AnswerRepository(ApplicationDbContext context)
        {
            _context = context;
        }       
        public async Task<IQueryable<Answer>> FindSolutions(Question question)
        {
            List<Answer> list = await _context.Answers.Where(x => x.QuestionId == question.Id && x.User == null && x.IsCorrect).ToListAsync();
            return list.AsQueryable();
        }

        public async Task<IQueryable<Answer>> FindOptions(int questionId)
        {
            var allAnswers = await GetAll();
            List<Answer> list = allAnswers.Where(x => x.QuestionId == questionId 
            //&& x.User==null
            ).ToList();
            return list.AsQueryable();
        }

        public async Task<IQueryable<Answer>> FindUserAnswersForQuestion(int questionId, string userId)
        {
            List<Answer> list = await _context.Answers.Where(x => x.QuestionId == questionId && x.UserId==userId).ToListAsync();
            return list.AsQueryable();
        }

        public async Task<Answer> GetById(int id)
        {
            return await _context.Answers.FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
