﻿using Data.Interfaces;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repos
{
    public class ExamLineRepository : GenericCrudRepository<ExamLine>, IExamLineRepository
    {
        public ExamLineRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public DatabaseFacade Database
        {
            get { return _context.Database; }
        }
                 

        public async Task<IQueryable<ExamLine>> FindByQuestionId(int questionId)
        {
            List<ExamLine> list=await _context.ExamLines.Where(x => x.QuestionId == questionId).ToListAsync();
            return list.AsQueryable();

        }

        public async Task<ExamLine> GetByKey(int lineid)
        {
            return await _context.ExamLines.FirstOrDefaultAsync(x=>x.LineId==lineid );
        }

        public async Task<IQueryable<ExamLine>> FindByExamId(int examId)
        {
            List<ExamLine> list = await _context.ExamLines.Where(x => x.ExamId == examId).ToListAsync();
            return list.AsQueryable();
        }
        
        /*public async Task DeleteExamLines(ICollection<ExamLine> list)
        {
            _context.ExamLines.RemoveRange(list);
        }*/
    }
}
