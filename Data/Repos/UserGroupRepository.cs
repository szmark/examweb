﻿using Data.Interfaces;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repos
{
    public class UserGroupRepository : GenericCrudRepository<UserGroup>, IUserGroupRepository
    {
        public UserGroupRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<UserGroup> GetById(int id)
        {
            return await _context.UserGroups.FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
