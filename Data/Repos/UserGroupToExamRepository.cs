﻿using Data.Interfaces;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repos
{
    public class UserGroupToExamRepository : GenericCrudRepository<UserGroupToExam>, IUserGroupToExamRepository
    {
        public UserGroupToExamRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IQueryable<UserGroupToExam>> FindByExam(int examid)
        {
            List<UserGroupToExam> list = await _context.UserToExam.Where(x => x.ExamId == examid).ToListAsync();
            return list.AsQueryable();
        }

        public async Task<UserGroupToExam> GetByKey(int groupid, int examid)
        {
            return await _context.UserToExam.FirstOrDefaultAsync(x => x.ExamId == examid && x.GroupId == groupid);
        }

        public async Task<IQueryable<UserGroupToExam>> FindByGroups(IQueryable<UserGroupLine> userGroupLines)
        {
            IQueryable<int> groupids = userGroupLines.Select(x => x.GroupId);
            List<UserGroupToExam> groupsToExam = await _context.UserToExam.Where( x => groupids.Contains(x.GroupId)).ToListAsync();
            return groupsToExam.AsQueryable();
        }
    }
}
