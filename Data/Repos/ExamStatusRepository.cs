﻿using Data.Interfaces;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repos
{
    public class ExamStatusRepository : GenericCrudRepository<ExamStatus>, IExamStatusRepository
    {
        public ExamStatusRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<ExamStatus> GetById(int id)
        {
            return await _context.Set<ExamStatus>().FirstOrDefaultAsync(u => u.Id == id);
        }
    }
}
