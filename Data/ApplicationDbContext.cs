﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Data.Models;
using Microsoft.AspNetCore.Identity;

namespace Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<ExamStatus> ExamStatus {get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<UserGroupLine> UserGroupLines { get; set; }
        public DbSet<Exam> Exams { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<ExamLine> ExamLines { get; set; }
        public DbSet<UserGroupToExam> UserToExam { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            //builder.Entity<ExamLine>().HasKey(table => new { table.ExamId, table.QuestionId });
            builder.Entity<UserGroupLine>().HasKey(x => new { x.GroupId, x.UserId });
            builder.Entity<UserGroupToExam>().HasKey(x =>new { x.GroupId,x.ExamId });

            base.OnModelCreating(builder);           
        }
    }
}
