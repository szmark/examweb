﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IAnswerRepository:IRepository<Answer>
    {
        Task<Answer> GetById(int id);
        Task<IQueryable<Answer>> FindSolutions(Question question);
        Task<IQueryable<Answer>> FindOptions(int questionId);
        Task<IQueryable<Answer>> FindUserAnswersForQuestion(int questionId,string userId);

    }
}
