﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IExamStatusRepository:IRepository<ExamStatus>
    {
        Task<ExamStatus> GetById(int id);
    }
}
