﻿using Data.Models;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IUserRepository
    {
        DatabaseFacade Database { get; }
        Task<ApplicationUser> FindById(string id);
        Task<ApplicationUser> FindByEmailAndPassword(string email,string password);
        Task<int> Create(ApplicationUser user);
        Task<int> Delete(ApplicationUser user);
        Task<int> Update(ApplicationUser user);
        Task<IReadOnlyCollection< ApplicationUser>> GetAll();
        //Task<ApplicationUser> FindAll();

    }
}
