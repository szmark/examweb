﻿using Data.Models;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IExamLineRepository: IRepository<ExamLine>
    {
        DatabaseFacade Database { get; }
        Task<ExamLine> GetByKey(int lineid);
        Task<IQueryable<ExamLine>> FindByExamId(int examId);
        Task<IQueryable<ExamLine>> FindByQuestionId(int questionId);       
    }
}
