﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IUserGroupRepository:IRepository<UserGroup>
    {
        Task<UserGroup> GetById(int id);
    }
}
