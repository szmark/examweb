﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IUserGroupToExamRepository:IRepository<UserGroupToExam>
    {
        Task<UserGroupToExam> GetByKey(int groupid, int examid);
        Task<IQueryable<UserGroupToExam>> FindByExam(int examid);
        Task<IQueryable<UserGroupToExam>> FindByGroups(IQueryable<UserGroupLine> userGroupLines);
    }
}
