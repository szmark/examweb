﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IExamRepository:IRepository<Exam>
    {
        DatabaseFacade Database { get; }
        Task<Exam> FindById(int id);
        Task<IQueryable<Exam>> FindByCreator(string creatorid);
    }
}
