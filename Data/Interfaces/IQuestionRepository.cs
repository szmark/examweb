﻿using Data.Models;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IQuestionRepository:IRepository<Question>
    {
        DatabaseFacade Database { get; }
        Task<Question> GetById(int id);
        Task<IQueryable<Question>> FindBySubject(Subject subject);
    }
}
