﻿using ApplicationService.ViewModels;
using Data;
using Data.Interfaces;
using Data.Models;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationService.Interfaces
{
    public interface IExamService
    {
        DatabaseFacade GetDatabase();
        Task<bool> InsertExam(Exam examHeader, List<ExamLine> examLines, List<UserGroupToExam> groupsToExam, List<Question> questions, List<Answer> answers);
        Task<bool> UpdateExam(Exam examHeader, List<ExamLine> examLines, List<UserGroupToExam> groupsToExam, List<Question> questions, List<Answer> answers);
        Task<bool> DeleteExam(Exam examHeader);
        Task<IEnumerable<Exam>> FindExamsByTeacher(string teacherid);
        Task<IEnumerable<Exam>> FindExamsByStudent(string studentid);
        Task<ExamStatus> GetExamStatusById(int id);
        Task<Subject> GetSubjectById(int id);
        Task<Question> GetQuestionById(int id);
        Task<Exam> GetExamById(int id);
        Task<bool> CreateQuestion(Question question);
        Task<bool> DeleteQuestion(Question question);
        Task<bool> CreateExamLine(ExamLine examLine);
        Task<bool> DeleteExamLine(ExamLine examLine);
        Task<bool> UpdateExamLine(ExamLine examLine);
        Task<bool> CreateExam(Exam exam);
        Task<bool> UpdateExam(Exam exam);
        Task<bool> AddGroupsToExam(UserGroupToExam groupToexam);
        Task<bool> DeleteGroupToExam(UserGroupToExam groupToexam);
        Task<IEnumerable<UserGroupToExam>> FindGroupToExamsByExam(Exam exam);
        Task<bool> UpdateGroupToExams(Exam exam, IEnumerable<UserGroupToExam> newlines);
        Task<bool> CreateAnswer(Answer answer);
        Task<bool> DeleteAnswer(Answer answer);
        Task<IEnumerable<Subject>> GetAllSubject();
        Task<IEnumerable<UserGroup>> GetAllGroups();
        Task<IEnumerable<ExamLine>> FindExamLines(int examId);
        Task<bool> DeleteExameLines(int examid);
        Task<bool> EvaluateExam(int examid, string userid);
        Task<IEnumerable<ExamStatViewModel>> GetDifficultExams(int db, bool ascending);
        Task<IEnumerable<QuestionStatViewModel>> GetDifficultQuestions(int db, bool ascending);
        Task<IEnumerable<UserStatViewModel>> GetSuccessfulUsers(int db, bool ascending, IEnumerable<ApplicationUser> users);
        Task<IEnumerable<UserStatViewModel>> GetUsersResultsByExamId(int id, IEnumerable<ApplicationUser> users);
        Task<IEnumerable<Answer>> FindSolutionAnswersByQuestion(int id);
    }
}
