﻿using Data.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationService.Interfaces
{
    public interface IUserService
    {
        Task<ApplicationUser> Autenticate(string username, string password);
        Task<ApplicationUser> FindUserById(string id);
        Task<IEnumerable<ApplicationUser>> GetAllUsers();
        Task<IdentityResult> UpdateUser(ApplicationUser user,string newrole);
        Task<IdentityResult> InsertUser(ApplicationUser user,string password,string rolename);
        Task<IdentityResult> DeleteUser(ApplicationUser user);
        Task<IEnumerable<IdentityRole>> GetRoles();
        Task<string> FindRoleByUser(ApplicationUser user);
        Task<IdentityRole> FindRoleByRoleName(string roleName);
    }
}
