﻿using ApplicationService.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationService.Interfaces
{
    public interface IEmailService
    {
        void SendEmail(Message message);
        Task<bool> SendEmailAsync(Message message);
    }
}
