﻿using ApplicationService.Interfaces;
using Data.Interfaces;
using Data.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace ApplicationService
{
    public class UserService:IUserService
    {
        IUserRepository _userRepository;
        UserManager<ApplicationUser> _userManager;
        RoleManager<IdentityRole> _roleManager;

        public UserService(IUserRepository userRepository, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, SmtpClient smtpClient)
        {
            _userRepository = userRepository;
            _userManager = userManager;
            this._roleManager = roleManager;
        }

        public async Task<ApplicationUser> Autenticate(string email, string password)
        {
            var user = _userRepository.FindByEmailAndPassword(email, password);

            // return null if user not found
            if (user == null)
               new ArgumentNullException("user");

            // authentication successful so generate jwt token
            /*var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            // remove password before returning
            //user.Password = null;*/

            return await user;
        }

        public async Task<IdentityResult> DeleteUser(ApplicationUser user)
        {
            //return await _userManager.DeleteAsync(user);
            var logins = await _userManager.GetLoginsAsync(user);
            var rolesForUser = await _userManager.GetRolesAsync(user);
            IdentityResult result = IdentityResult.Success;

            using (var transaction = _userRepository.Database.BeginTransaction())
            {
                //IdentityResult result = IdentityResult.Success;
                foreach (var login in logins)
                {
                    result = await _userManager.RemoveLoginAsync(user, login.LoginProvider, login.ProviderKey);
                    if (result != IdentityResult.Success)
                        return result;
                }
                if (result == IdentityResult.Success)
                {
                    foreach (var item in rolesForUser)
                    {
                        result = await _userManager.RemoveFromRoleAsync(user, item);
                        if (result != IdentityResult.Success)
                            return result;
                    }
                }
                if (result == IdentityResult.Success)
                {
                    result = await _userManager.DeleteAsync(user);
                    if (result == IdentityResult.Success)
                        transaction.Commit(); //only commit if user and all his logins/roles have been deleted                     
                }
            }
            return result;
        }

        public async Task<ApplicationUser> FindUserById(string id)
        {
            var user= await _userRepository.FindById(id);
            if (user==null)
            {
                throw new ArgumentException("user was not found.");
            }
            return user;
        }

        public async  Task<string> FindRoleByUser(ApplicationUser user)
        {
            IList<string> roles= await _userManager.GetRolesAsync(user);
            return roles.ToList().FirstOrDefault();
        }

        public async Task<IEnumerable<ApplicationUser>> GetAllUsers()
        {
            return await _userRepository.GetAll();
        }

        public async Task<IdentityResult> InsertUser(ApplicationUser user,string password,string rolename)
        {
            IdentityResult result = IdentityResult.Success;
            using (var transaction = _userRepository.Database.BeginTransaction())
            {
                result = await _userManager.CreateAsync(user,password);
                if (result != IdentityResult.Success)
                {
                    return result;
                }
                result = await _userManager.AddToRoleAsync(user, rolename);
                if (result != IdentityResult.Success)
                {
                    return result;
                }
                else
                {
                    transaction.Commit();
                }                
            }
            return result;
        }

        public async Task<IdentityResult> UpdateUser(ApplicationUser user, string newrole)
        {
            IdentityResult result = IdentityResult.Success;
            string roleForUser = ((List<string>)await _userManager.GetRolesAsync(user)).FirstOrDefault();
            using (var transaction = _userRepository.Database.BeginTransaction())
            {
                result = await _userManager.UpdateAsync(user);
                if (result != IdentityResult.Success)
                {
                    return result;
                }
                result = await _userManager.RemoveFromRoleAsync(user, roleForUser);
                if (result != IdentityResult.Success)
                {
                    return result;
                }
                result = await _userManager.AddToRoleAsync(user,newrole);
                if (result != IdentityResult.Success)
                {
                    return result;
                }
                else
                {
                    transaction.Commit();
                }
            }
                return result;
        }

        public async Task<IEnumerable<IdentityRole>> GetRoles()
        {
            return await  _roleManager.Roles.ToAsyncEnumerable().ToList();
        }

        public async Task<IdentityRole> FindRoleByRoleName(string roleName)
        {
          var role= await  _roleManager.FindByNameAsync(roleName);
            if (role==null)
            {
                throw new ArgumentNullException("role was not found.");
            }
            return role;
        }       
    }
}
