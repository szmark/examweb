﻿using ApplicationService.BusinessLogic;
using ApplicationService.Interfaces;
using ApplicationService.ViewModels;
using Data;
using Data.Interfaces;
using Data.Models;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationService.services
{
    public class ExamService : IExamService
    {
        IExamRepository _examRepository;
        IExamLineRepository _examLineRepository;
        IUserGroupToExamRepository _userGroupToExamRepository;
        IQuestionRepository _questionRepository;
        IAnswerRepository _answerRepository;
        IUserGroupLineRepository _userGroupLineRepository;
        IExamStatusRepository _examStatusRepository;
        ISubjectRepository _subjectRepository;
        IUserGroupRepository _userGroupRepository;       

        public ExamService(IExamRepository examRepository,IExamLineRepository examLineRepository, IUserGroupToExamRepository userGroupToExamRepository, IQuestionRepository questionRepository,IAnswerRepository answerRepository, IUserGroupLineRepository userGroupLineRepository, IExamStatusRepository examStatusRepository,ISubjectRepository subjectRepository, IUserGroupRepository userGroupRepository)
        {
            _examRepository = examRepository;
            _examLineRepository= examLineRepository;
            _userGroupToExamRepository = userGroupToExamRepository;
            _questionRepository = questionRepository;
            _answerRepository = answerRepository;
            _userGroupLineRepository= userGroupLineRepository;
            _examStatusRepository = examStatusRepository;
            _subjectRepository = subjectRepository;
            _userGroupRepository = userGroupRepository;
        }

        public DatabaseFacade GetDatabase()
        {
            return _examRepository.Database;
        }

        public Task<bool> DeleteExam(Exam examHeader)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> CreateExam(Exam exam)
        {
            try
            {
                int changes = await _examRepository.Create(exam);
                if (changes < 1)
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
            return true;
        }

        public async Task<bool> UpdateExam(Exam exam)
        {
            try
            {
                int changes = await _examRepository.Update(exam);
                if (changes < 1)
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
            return true;
        }
     

        public async Task<bool> DeleteAnswer(Answer answer)
        {
            try
            {
                int changes = await _answerRepository.Delete(answer);
                if (changes < 1)
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
            return true;
        }

        public async Task<bool> CreateAnswer(Answer answer)
        {
            try
            {
                int changes = await _answerRepository.Create(answer);
                if (changes < 1)
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
            return true;
        }

        public async Task<bool> DeleteQuestion(Question question)
        {
            try
            {
                int changes = await _questionRepository.Delete(question);
                if (changes < 1)
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
            return true;
        }

        public async Task<bool> DeleteExamLine(ExamLine examLine)
        {
            try
            {
                int changes = await _examLineRepository.Delete(examLine);
                if (changes < 1)
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
            return true;
        }

        public async Task<bool> UpdateExamLine(ExamLine examLine)
        {
            try
            {
                int changes = await _examLineRepository.Update(examLine);
                if (changes < 1)
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
            return true;
        }


        public async Task<bool> AddGroupsToExam(UserGroupToExam groupToexam)
        {
            try
            {
               int changes =await _userGroupToExamRepository.Create(groupToexam);
                if (changes < 1)
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
            return true;
        }

        public async Task<bool> DeleteGroupToExam(UserGroupToExam groupToexam)
        {
            try
            {
                int changes =await _userGroupToExamRepository.Delete(groupToexam);
                if (changes < 1)
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
            return true;
        }

        public async Task<IEnumerable<UserGroupToExam>> FindGroupToExamsByExam(Exam exam)
        {
            IQueryable<UserGroupToExam> result = await _userGroupToExamRepository.FindByExam(exam.Id);
            return result.ToList();
        }

        public async Task<bool> UpdateGroupToExams(Exam exam, IEnumerable<UserGroupToExam> newlines)
        {
            IEnumerable<UserGroupToExam> oldLines =await FindGroupToExamsByExam(exam);
            foreach (var line in oldLines)
            {
                if(!await DeleteGroupToExam(line))
                {
                    return false;
                }                
            }
            foreach (var line in newlines)
            {
                if (!await AddGroupsToExam(line))
                {
                    return false;
                }
            }
            return true;

            
        }

        public async Task<IEnumerable<Exam>> FindExamsByStudent(string studentid)
        {
            IQueryable<UserGroupLine> userGroupLines = await  _userGroupLineRepository.FindByUser(studentid);
            IQueryable<UserGroupToExam> groupsToExam = await _userGroupToExamRepository.FindByGroups(userGroupLines);
            IQueryable<int> examids = groupsToExam.Select(y =>y.ExamId );
            return _examRepository.GetAll().Result.AsQueryable().Where(x=>examids.Contains(x.Id) && x.Status.Id>1).ToList();
            
             
            
        }

        public async Task<IEnumerable<Exam>> FindExamsByTeacher(string teacherid)
        {
            try
            {
                IQueryable<Exam> exams = await _examRepository.FindByCreator(teacherid);
                return exams.ToList();
            }
            catch(Exception e)
            {
                Debug.WriteLine(e.Message);
                return null;
            }
            
        }

        public async Task<ExamStatus> GetExamStatusById(int id)
        {
            try
            {
                return await _examStatusRepository.GetById(id);
            }
            catch(Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        public async Task<Subject> GetSubjectById(int id)
        {
            try
            {
                return await _subjectRepository.GetById(id);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        public async Task<Question> GetQuestionById(int id)
        {
            return await  _questionRepository.GetById(id);
        }

        public async Task<Exam> GetExamById(int id)
        {
            try
            {
                var exam = await _examRepository.FindById(id);
                return exam;
            }
            catch
            {
                return null;
            }
        }

        public async Task<bool> CreateQuestion(Question question)
        {
            try
            {
                int changes = await _questionRepository.Create(question);
                if (changes < 1)
                {
                    return false;
                }
            }
            catch(Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
            return true;
        }     
        public async Task<bool> CreateExamLine(ExamLine examLine)
        {
            try
            {
                int changes = await _examLineRepository.Create(examLine);
                if (changes < 1)
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
            return true;
        }

        public async Task<IEnumerable<Subject>> GetAllSubject()
        {
            try
            {
                var subjects = await _subjectRepository.GetAll();
                return subjects.ToList();
            }
            catch(Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        //ezt törölni kell majd valószinű
        public async Task<bool> InsertExam(Exam examHeader, List<ExamLine> examLines, List<UserGroupToExam> groupsToExam, List<Question> questions, List<Answer> answers)
        {
            int changes;
            using (var transaction = _examRepository.Database.BeginTransaction())
            {
                try
                {
                    changes = await _examRepository.Create(examHeader);
                    if (changes < 1)
                    {
                        return false;
                    }
                    //add groups to exam
                    foreach (var grouptoExam in groupsToExam)
                    {
                        changes = await _userGroupToExamRepository.Create(grouptoExam);
                        if (changes < 1)
                        {
                            return false;
                        }
                    }
                    //add new answers and questions                  
                    foreach (var question  in questions)
                    {
                        changes = await _questionRepository.Create(question);
                        if (changes < 1)
                        {
                            return false;
                        }
                    }
                    foreach (var answer in answers)
                    {
                        changes = await _answerRepository.Create(answer);
                        if (changes < 1)
                        {
                            return false;
                        }
                    }
                    //add new exam lines to exam
                    foreach (var line in examLines)
                    {
                        changes= await _examLineRepository.Create(line);
                        if (changes < 1)
                        {
                            return false;
                        }
                    }
                    transaction.Commit();
                }
                catch(Exception e)
                {
                    Debug.WriteLine(e.Message);
                    return false;
                }               
            }
            return true;
        }

        //ezt át kell gondolni
        public async Task<bool> UpdateExam(Exam examHeader, List<ExamLine> examLines, List<UserGroupToExam> groupsToExam, List<Question> questions, List<Answer> answers)
        {
            int changes;
            using (var transaction = _examRepository.Database.BeginTransaction())
            {
                try
                {
                    changes = await _examRepository.Update(examHeader);
                    if (changes < 1)
                    {
                        return false;
                    }
                    //update groups to exam
                    foreach (var grouptoExam in groupsToExam)
                    {
                        changes = await _userGroupToExamRepository.Update(grouptoExam);
                        if (changes < 1)
                        {
                            return false;
                        }
                    }
                    //update answers and questions
                    foreach (var answer in answers)
                    {
                        changes = await _answerRepository.Update(answer);
                        if (changes < 1)
                        {
                            return false;
                        }
                    }
                    foreach (var question in questions)
                    {
                        changes = await _questionRepository.Update(question);
                        if (changes < 1)
                        {
                            return false;
                        }
                    }
                    //update exam lines to exam
                    foreach (var line in examLines)
                    {
                        changes = await _examLineRepository.Update(line);
                        if (changes < 1)
                        {
                            return false;
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                    return false;
                }
            }
            return true;
        }

        public async Task<IEnumerable<UserGroup>> GetAllGroups()
        {
            try
            {
                var groups = await _userGroupRepository.GetAll();
                return groups.ToList();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        public async Task<IEnumerable<ExamLine> > FindExamLines(int examId)
        {
            try
            {
                var exam_lines = await _examLineRepository.FindByExamId(examId);
                return exam_lines.ToList();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        public async Task<bool> DeleteExameLines(int examid)
        {
            try
            {
                //bool result = true;
                var exam = await GetExamById(examid);
                /*foreach (var line in exam.ExamLines)
                {
                    line.Question.Answers.Clear();
                    result = await DeleteQuestion(line.Question);
                    if (!result)
                    {
                        return result;
                    }
                }*/
                exam.ExamLines.Clear();
            }
            catch(Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
            return true;
        }

        public async Task<bool> EvaluateExam(int examid, string userid)
        {
            Exam exam = await GetExamById(examid);
            bool result = false;
            using (var transaction = _examRepository.Database.BeginTransaction())
            {               
                IEnumerable<ExamLine> examlines = await _examLineRepository.GetAll();
                //IEnumerable<ExamLine>  solutionLinesByExamId = examlines.Where(x => x.ExamId == examid && x.User == null);
                IEnumerable<ExamLine> userLinesByExamId = examlines.Where(x => x.ExamId == examid && x.UserId == userid);
                foreach (var line in userLinesByExamId)
                {
                    double? value = CalcPTSToQuestionLogic(line.Question.Answers.Where(y => y.UserId == userid), line.Question.Answers.Where(x => x.User == null), (EvaluationType) exam.EvaluateType, (double)line.Value); //be vna égetve 0 egyenlőre                
                    line.Value = value;
                    result = await UpdateExamLine(line);
                    if(!result)
                    {
                        return result;
                    }
                }
                transaction.Commit();   
            return result;
            }

        }
        public async Task<IEnumerable<UserStatViewModel>> GetUsersResultsByExamId(int id,IEnumerable<ApplicationUser> users)
        {
            IEnumerable<UserStatViewModel> model;
            try
            {
                ExamLineLogic bl = new ExamLineLogic();
                var allExamLines = await FindExamLines(id);
                var usersolutions = bl.FindUserSolutions(allExamLines);
                model = bl.GroupExamLinesByUserId(usersolutions, allExamLines, users);
            }
            catch(Exception e)
            {
                Debug.Write(e);
                return null;
            }
            return model;
        }

        public async Task<IEnumerable<Answer>> FindSolutionAnswersByQuestion(int id)
        {
            AnswerLogic bl = new AnswerLogic();
            return await  bl.FindSolutionsForQuestion(_answerRepository,id);
        }


        //Business logic
        public IQueryable<ExamLine> FindUserExamLineLogic(IQueryable<ExamLine> examlines ,int examid, string userid)
        {
            return examlines.Where(x => x.ExamId == examid && x.UserId == userid);
        }       
             
        public double? CalcPTSToQuestionLogic(IEnumerable<Answer> userAnswers, IEnumerable<Answer> Solutions, EvaluationType type,double maxpont)
        {
            Answer sol = Solutions.ToList()[0];
            if (sol.QuestionType==1)
            {
                return null;
            }
            else if (sol.QuestionType == 2)
            {
                Answer userAnswer = userAnswers.ToList()[0];
                if (sol.TrueOrFalse== userAnswer.TrueOrFalse)
                {
                    return maxpont;
                }
                else
                {
                    return 0;
                }
            }
            double pts = 0;
            if(type==EvaluationType.Normal)
            {
                IEnumerable<Answer> correctUserAnswers = userAnswers.Where(x => x.IsCorrect);
                IEnumerable<Answer> correctSolutions = Solutions.Where(x => x.IsCorrect);

                if (correctUserAnswers.Count() == correctSolutions.Count())
                {
                    if (correctSolutions.Count() == 0)
                    {
                        pts = maxpont;
                    }
                    else
                    {
                        foreach (var CorrectUserAnswer in correctUserAnswers)
                        {
                            if (correctSolutions.Where(a => a.TextAnswer == CorrectUserAnswer.TextAnswer).Count() == 0)
                            {
                                return 0;
                            }
                        }
                        pts = maxpont;
                    }

                }
                else
                {
                    pts = 0;
                }
            }
            if (type == EvaluationType.HavePartial)
            {
                double partPoint = maxpont / Solutions.Count();
                foreach (var UserAnswer in userAnswers)
                {
                    if (Solutions.Where(a => a.TextAnswer == UserAnswer.TextAnswer && a.IsCorrect==UserAnswer.IsCorrect).Count() != 0)
                    {
                        pts += partPoint;
                    }
                }
            }
            if (type == EvaluationType.HaveMinus)
            {
                double minusPont = -1*(maxpont / 2);
                IEnumerable<Answer> correctUserAnswers = userAnswers.Where(x => x.IsCorrect);
                IEnumerable<Answer> correctSolutions = Solutions.Where(x => x.IsCorrect);
                if(correctUserAnswers.Count()==0)
                {
                    if(correctSolutions.Count()==0)
                    {
                        pts = maxpont;
                    }
                    else
                    {
                        pts = 0;
                    }
                }
                else
                {
                    foreach (var UserAnswer in userAnswers)
                    {
                        if (Solutions.Where(a => a.TextAnswer == UserAnswer.TextAnswer && a.IsCorrect == UserAnswer.IsCorrect).Count() == 0)
                        {
                            return minusPont;

                        }
                    }
                    pts = maxpont;
                }
            }

                return pts;
        }

        public async Task<IEnumerable<Answer>> FindSolutionsByQuestion(Question question)
        {
            try
            {
                IQueryable<Answer> solutions = await _answerRepository.FindSolutions(question);
                return solutions;
            }
            catch (Exception e)
            {
                Debug.Write(e);
                return null;
            }
        }

        public async Task<IEnumerable<ExamStatViewModel>> GetDifficultExams(int db, bool ascending)
        {
            var exams =await  _examRepository.GetAll();
            ExamLineLogic bl = new ExamLineLogic();
            IEnumerable<ExamStatViewModel> res;
            if (ascending)
            {
                 res = await bl.TopXMostDifficultExam(db, _examLineRepository, exams.ToList());
            }
            else
            {
                res = await bl.TopXLeastDifficultExam(db, _examLineRepository, exams.ToList());
            }
            return res;
        }

        public async Task<IEnumerable<QuestionStatViewModel>> GetDifficultQuestions(int db, bool ascending)
        {
            ExamLineLogic bl = new ExamLineLogic();
            IEnumerable<QuestionStatViewModel> res;
            if (ascending)
            {
                res = await bl.TopXLeastEasyQuestion(db, _examLineRepository);
            }
            else
            {
                res = await bl.TopXMostEasyQuestion(db, _examLineRepository);
            }
            return res;
        }

        public async Task<IEnumerable<UserStatViewModel>> GetSuccessfulUsers(int db, bool ascending, IEnumerable<ApplicationUser> users)
        {           
            ExamLineLogic bl = new ExamLineLogic();
            IEnumerable<UserStatViewModel> res;
            if (ascending)
            {
                res = await bl.TopXLeastSuccesfulUsers(db, _examLineRepository,users.ToList());
            }
            else
            {
                res = await bl.TopXMostSuccesfulUsers(db, _examLineRepository, users.ToList());
            }
            return res;
        }


    }
}
