﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationService.ViewModels
{
    public class ExamStatViewModel
    {
        public int ExamID { get; set; }
        public string Title { get; set; }
        public double AvgPTS { get; set; }
        public double AvgPercent { get; set; }
    }
}
