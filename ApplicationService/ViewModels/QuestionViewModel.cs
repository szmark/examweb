﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationService.ViewModels
{
    public class QuestionViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double? Value { get; set; }
        public AnswerViewModel[] Answers { get; set; }

        public double? MaxValue { get; set; }
    }
}
