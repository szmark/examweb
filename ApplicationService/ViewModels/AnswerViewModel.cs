﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationService.ViewModels
{
    public class AnswerViewModel
    {
        public int Id { get; set; }
        public string TextAnswer { get; set; }
        public bool TrueOrFalse { get; set; }
        public string AnswerA { get; set; }
        public string AnswerB { get; set; }
        public bool IsCorrect { get; set; }       
        public string UserId { get; set; }
        public int QuestionType { get; set; }
    }
}
