﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationService.ViewModels
{
    public class UserStatViewModel
    {
        public string UserID { get; set; }
        public string Name { get; set; }
        public double AvgPTS { get; set; }
        public double AvgPercent { get; set; }
        public double PtsSum { get; set; }        
    }
}
