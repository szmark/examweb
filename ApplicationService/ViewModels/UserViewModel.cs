﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationService.ViewModels
{
    public class UserViewModel
    {
        public string Id { get; set; }
        /*public string FirstName { get; set; }
        public string LastName { get; set; } */
        [Required]
        public string Email { get; set; }
        [Required]
        public string Role { get; set; }
        public string Password { get; set; }
    }
}
