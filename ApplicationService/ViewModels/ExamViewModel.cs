﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationService.ViewModels
{
    public class ExamViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime FromDate { get; set; }
        public int ToDate { get; set; }
        public SubjectViewModel Subject { get; set; }
        public ExamStatusViewModel Status { get; set; }
        public UserViewModel User { get; set; }
        public QuestionViewModel[] Questions { get; set; }
        public UserGroupViewModel[] UserGroup { get; set; }

        public int  EvaluateType { get; set; }

    }
}
