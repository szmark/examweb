﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationService.ViewModels
{
    public class QuestionStatViewModel
    {
        public int QuestionID { get; set; }
        public string QuestionName { get; set; }
        public double AvgPTS { get; set; }
        public double AvgPercent { get; set; }
    }
}
