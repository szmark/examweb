﻿using Data.Interfaces;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationService.BusinessLogic
{
    public class AnswerLogic
    {

        public double CalcPTSToQuestion(IEnumerable<Answer> userAnswers, IEnumerable<Answer> Solutions, EvaluationType type, double maxpont)
        {
            double pts = 0;
            if (type == EvaluationType.Normal)
            {
                IEnumerable<Answer> correctUserAnswers = userAnswers.Where(x => x.IsCorrect);
                IEnumerable<Answer> correctSolutions = Solutions.Where(x => x.IsCorrect);

                if (correctUserAnswers.Count() == correctSolutions.Count())
                {
                    if (correctSolutions.Count() == 0)
                    {
                        pts = maxpont;
                    }
                    else
                    {
                        foreach (var CorrectUserAnswer in correctUserAnswers)
                        {
                            if (correctSolutions.Where(a => a.TextAnswer == CorrectUserAnswer.TextAnswer).Count() == 0)
                            {
                                return 0;
                            }
                        }
                        pts = maxpont;
                    }

                }
                else
                {
                    pts = 0;
                }
            }
            if (type == EvaluationType.HavePartial)
            {
                double partPoint = maxpont / Solutions.Count();
                foreach (var UserAnswer in userAnswers)
                {
                    if (Solutions.Where(a => a.TextAnswer == UserAnswer.TextAnswer && a.IsCorrect == UserAnswer.IsCorrect).Count() != 0)
                    {
                        pts += partPoint;
                    }
                }
            }
            if (type == EvaluationType.HaveMinus)
            {
                double minusPont = -1 * (maxpont / 2);
                IEnumerable<Answer> correctUserAnswers = userAnswers.Where(x => x.IsCorrect);
                IEnumerable<Answer> correctSolutions = Solutions.Where(x => x.IsCorrect);
                if (correctUserAnswers.Count() == 0)
                {
                    if (correctSolutions.Count() == 0)
                    {
                        pts = maxpont;
                    }
                    else
                    {
                        pts = 0;
                    }
                }
                else
                {
                    foreach (var UserAnswer in userAnswers)
                    {
                        if (Solutions.Where(a => a.TextAnswer == UserAnswer.TextAnswer && a.IsCorrect == UserAnswer.IsCorrect).Count() == 0)
                        {
                            return minusPont;

                        }
                    }
                    pts = maxpont;
                }
            }

            return pts;
        }

        public async Task< IEnumerable<Answer> > FindUserAnswersForQuestion(IAnswerRepository answerRepo, int questionId, string userid)
        {
            var res = await GetAllAnswer(answerRepo);
            return res.Where(x => x.UserId==userid && x.QuestionId == questionId).ToList();

        }

        public async  Task<IEnumerable<Answer>> FindSolutionsForQuestion(IAnswerRepository answerRepo,int questionId)
        {
            var res = await GetAllAnswer(answerRepo);
            return res.Where(x => x.User == null && x.QuestionId == questionId).ToList();
            
        }      

        public async Task<IEnumerable<Answer>> GetAllAnswer(IAnswerRepository answerRepo)
        {
            var res=await answerRepo.GetAll();
            return res.ToList();
        } 
        
        public bool? AnswerRealCorrect(Answer a, List<Answer> solutions)
        {
            if(a.QuestionType==(int) QuestionType.Option)
            {
                var solution = solutions.Where(x => x.QuestionId == a.QuestionId && x.TextAnswer == a.TextAnswer).FirstOrDefault();
                if (solution !=null && solution.IsCorrect == a.IsCorrect)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if( a.QuestionType == (int)QuestionType.TrueOrFalse)
            {
                var solution = solutions.Where(x => x.QuestionId == a.QuestionId).FirstOrDefault();
                if (solution != null && solution.TrueOrFalse == a.TrueOrFalse)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return null;
            }
        }
    }
}
