﻿using ApplicationService.ViewModels;
using Data;
using Data.Interfaces;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationService.BusinessLogic
{
    public class ExamLineLogic
    {
        public async Task <IEnumerable<QuestionStatViewModel>> TopXMostEasyQuestion(int topx,IExamLineRepository examLineRepo)
        {
            var allExamLines = await examLineRepo.GetAll();
            var usersolutions = allExamLines.Where(x => x.User != null);
            List<QuestionStatViewModel> res = new List<QuestionStatViewModel>();
            var group = usersolutions.GroupBy(x => x.QuestionId);
            foreach (var g  in group)
            {
                res.Add(new QuestionStatViewModel()
                {
                    QuestionID = g.Key,                   
                    AvgPTS = g.Average(x => (double)x.Value),
                    AvgPercent = g.Average(x => (double)(x.Value / GetAchiveAbleValue(allExamLines.ToList(), g.Key,x.ExamId)))
                });
            }
            int db = res.Count() < topx ? res.Count() : topx;
            return res.OrderByDescending(x => x.AvgPercent).Take(db).ToList();

            
        }

        public async Task<IEnumerable<QuestionStatViewModel>> TopXLeastEasyQuestion(int topx, IExamLineRepository examLineRepo)
        {
            var allExamLines = await examLineRepo.GetAll();
            var usersolutions = allExamLines.Where(x => x.User != null && x.Value!=null);
            List<QuestionStatViewModel> res = new List<QuestionStatViewModel>();
            var group = usersolutions.GroupBy(x => x.QuestionId);
            foreach (var g in group)
            {
                res.Add(new QuestionStatViewModel()
                {
                    QuestionID = g.Key,
                    QuestionName=g.Where(x=>x.QuestionId==g.Key).FirstOrDefault().QuestionName,
                    AvgPTS = g.Average(x => (double)x.Value),
                    AvgPercent = g.Average(x =>(double)( x.Value / GetAchiveAbleValue(allExamLines.ToList(), g.Key,x.ExamId)))
                });
            }
            int db = res.Count() < topx ? res.Count() : topx;
            return res.OrderBy(x => x.AvgPercent).Take(db).ToList();
        }

        public double? GetAchiveAbleValue(List<ExamLine> allExamline,int questionid,int examId)
        {
            return allExamline.Where(x =>x.ExamId==examId && x.QuestionId == questionid && x.User == null).FirstOrDefault().Value;
        }

        public async Task<IEnumerable<ExamStatViewModel>> TopXMostDifficultExam(int topx, IExamLineRepository examLineRepo, IEnumerable<Exam> exams)
        {
            var allExamLines = await examLineRepo.GetAll();
            var usersolutions = FindUserSolutions(allExamLines);
            var res = GroupExamLinesByExamId(usersolutions, allExamLines,exams);
            int db = res.Count() < topx ? res.Count() : topx;
            return res.OrderBy(x => x.AvgPercent).Take(db).ToList();
        }

        public async Task<IEnumerable<ExamStatViewModel>> TopXLeastDifficultExam(int topx, IExamLineRepository examLineRepo, IEnumerable<Exam> exams)
        {
            var allExamLines = await examLineRepo.GetAll();
            var usersolutions = FindUserSolutions(allExamLines);
            var res = GroupExamLinesByExamId(usersolutions, allExamLines,exams);
            int db = res.Count() < topx ? res.Count() : topx;
            return res.OrderByDescending(x => x.AvgPercent).Take(db).ToList();
        }

        public IEnumerable<ExamLine> FindUserExamLineByUserAndExam(IEnumerable<ExamLine> examlines, int examid, string userid)
        {
            return examlines.Where(x => x.ExamId == examid && x.UserId == userid);
        }

        public IEnumerable<ExamLine> FindUserSolutions(IEnumerable<ExamLine> allExamLines)
        {
            return allExamLines.Where(x => x.User != null);
        }

        public IEnumerable<ExamStatViewModel> GroupExamLinesByExamId(IEnumerable<ExamLine> usersolutions, IEnumerable<ExamLine> allExamLines, IEnumerable<Exam> exams)
        {
            List<ExamStatViewModel> res = new List<ExamStatViewModel>();
            var group = usersolutions.GroupBy(x => x.ExamId);
            foreach (var g in group)
            {
                res.Add(new ExamStatViewModel()
                {
                    ExamID = g.Key,
                    Title=exams.Where(e=>e.Id==g.Key).FirstOrDefault().Title,
                    AvgPercent = g.Average(x =>(double) ( x.Value / GetAchiveAbleValue(allExamLines.ToList(), x.QuestionId,x.ExamId)))
                });
            }
            return res;
        }

        public IEnumerable<UserStatViewModel> GroupExamLinesByUserId(IEnumerable<ExamLine> usersolutions, IEnumerable<ExamLine> allExamLines, IEnumerable<ApplicationUser>users)
        {
            List<UserStatViewModel> res = new List<UserStatViewModel>();
            var group = usersolutions.GroupBy(x => x.UserId);
            foreach (var g in group)
            {
                res.Add( new UserStatViewModel()
                {
                    UserID = g.Key,
                    Name=users.Where(u=>u.Id==g.Key).FirstOrDefault().UserName,
                    AvgPercent = g.Average(x => (double)(x.Value / GetAchiveAbleValue(allExamLines.ToList(), x.QuestionId,x.ExamId))),
                    PtsSum=g.Sum(x=> (double)x.Value), 
                    //MaxPont= GetAchiveAbleValue(allExamLines.ToList(), x.QuestionId, x.ExamId)
                });
            }
            return res;
        }

        public async Task<IEnumerable<UserStatViewModel>> TopXMostSuccesfulUsers(int topx, IExamLineRepository examLineRepo,IEnumerable<ApplicationUser> users)
        {
            var allExamLines = await examLineRepo.GetAll();
            var usersolutions = FindUserSolutions(allExamLines);
            //var users = await userRepo.GetAll();
            var res = GroupExamLinesByUserId(usersolutions, allExamLines,users);
            int db = res.Count() < topx ? res.Count() : topx;
            return res.OrderByDescending(x => x.AvgPercent).Take(db).ToList();
        }

        public async Task<IEnumerable<UserStatViewModel>> TopXLeastSuccesfulUsers(int topx, IExamLineRepository examLineRepo, IEnumerable<ApplicationUser> users)
        {
            var allExamLines = await examLineRepo.GetAll();
            var usersolutions = FindUserSolutions(allExamLines);
            //var users = await userRepo.GetAll();
            var res = GroupExamLinesByUserId(usersolutions, allExamLines, users);
            int db = res.Count() < topx ? res.Count() : topx;
            return res.OrderBy(x => x.AvgPercent).Take(db).ToList();
        }
    }
}
