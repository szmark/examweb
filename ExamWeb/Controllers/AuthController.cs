﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ApplicationService.Interfaces;
using ApplicationService.ViewModels;
using Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace ExamWeb.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IUserService _userservice;
        private readonly IEmailService _emailService;

        public AuthController(UserManager<ApplicationUser> userManager,IUserService userservice, IConfiguration configuration, IEmailService emailService)
        {
            _userManager = userManager;
            _userservice = userservice;
            _configuration = configuration;
            _emailService = emailService;
        }

        [AllowAnonymous]
        // /register
        [Route("register")]
        [HttpPost]
        public async Task<ActionResult> InsertUser([FromBody] UserViewModel model)
        {
            var user = new ApplicationUser
            {
                Email = model.Email,
                UserName = model.Email,
                SecurityStamp = Guid.NewGuid().ToString()
            };
            /*var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, model.Role);
            }
            */
            string password = model.Password == string.Empty ? GeneratePassword() : model.Password;
            Debug.WriteLine("username: "+user.UserName+"password: "+password);
            var result= await _userservice.InsertUser(user,password, model.Role);
            await SendMail(user.UserName, password);
            if (result.Succeeded)
            {
                return Ok(new { Username = user.UserName });
            }
            else
            {
                return Forbid();
            }
        }

        [AllowAnonymous]
        [Route("login")] // /login
        [HttpPost]
        public async Task<ActionResult> Login([FromBody] LoginViewModel model)
        {
            
            try
            {
                var user = await _userManager.FindByNameAsync(model.Username);
            
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                var roles = await _userManager.GetRolesAsync(user);
                var claim= new List<Claim>();
                claim.Add(new Claim(JwtRegisteredClaimNames.Sub, user.UserName));
                //claim.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
                foreach (var role in roles)
                {
                    claim.Add(new Claim(ClaimTypes.Role, role));
                }               
                var signinKey = new SymmetricSecurityKey(
                  Encoding.UTF8.GetBytes(_configuration["Jwt:SigningKey"]));

                int expiryInMinutes = Convert.ToInt32(_configuration["Jwt:ExpiryInMinutes"]);

                var token = new JwtSecurityToken(
                  issuer: _configuration["Jwt:Site"],
                  audience: _configuration["Jwt:Site"],
                  expires: DateTime.UtcNow.AddMinutes(expiryInMinutes),
                  signingCredentials: new SigningCredentials(signinKey, SecurityAlgorithms.HmacSha256),
                  claims: claim.ToArray()
                );

                return Ok(
                  new
                  {
                      id = user.Id,
                      username = user.UserName,
                      email = user.Email,
                      role = roles.FirstOrDefault(),
                      token = new JwtSecurityTokenHandler().WriteToken(token),
                      expiration = token.ValidTo                    
                  });
            }
            return Unauthorized();
            }
            catch (Exception e)
            {
                Debug.WriteLine("hiba: " + e.Message);
                return Unauthorized();
            }
        }
        
        [Route("users")]
        [HttpGet]
        // GET: api/student
        public async Task<ActionResult> GetALLUsers()
        {
            //TEST
            //var message = new Message(new string[] { "szekacsmark@gmail.com" }, "Test email", "This is the content from our email.",null);
            //_emailService.SendEmail(message);
            //TEST
            var users = await _userservice.GetAllUsers();
            if (users!=null)
            {
                 var res=users.Select(async u => new
                {
                    id = u.Id,
                    username = u.UserName,
                    role = (await _userManager.GetRolesAsync(u)).FirstOrDefault()
                 });
                var result = await Task.WhenAll(res);
                return Ok(result);
            }
            else
            {
                return Forbid();
            }
        }
        
        [Route("users/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetUserById(string id)
        {
            ApplicationUser user;
            string roleName;
            try
            {
                user = await _userservice.FindUserById(id);
                roleName = await _userservice.FindRoleByUser(user);
            }
            catch (ArgumentNullException)
            {
                return Forbid();
            }
            //user.PasswordHash = null;
            return Ok(
                new
                {
                    id = user.Id,
                    username = user.UserName,
                    email = user.Email,
                    role = roleName
                });
        }
        [Authorize(Roles ="Admin")]
        [Route("user")]
        [HttpPut]
        public async Task<ActionResult> ModifyUser([FromBody] UserViewModel model)
        {
            var user= await _userservice.FindUserById(model.Id);
            user.Email = model.Email;
            user.UserName = model.Email;           
            var result =await _userservice.UpdateUser(user,model.Role);
            //Manager.RemoveFromRole(user.Id, oldRoleName);
            //Manager.AddToRole(user.Id, role);
            if (result.Succeeded)
            {
                return Ok(new { user.Email });
            }
            else
            {
                return Forbid();
            }

        }
        [Authorize(Roles = "Admin")]
        [Route("user/{id}")]
        [HttpDelete]
        public async Task<ActionResult> DeleteUser(string Id)
        {
            var user = await _userservice.FindUserById(Id);
            var result= await _userservice.DeleteUser(user);
            if (result.Succeeded)
            {
                return Ok(new { user.Email }); 
            }
            return Forbid();
        }
        [Authorize(Roles = "Admin")]
        [Route("roles")]
        [HttpGet]
        public async Task<ActionResult> GetAllRoles()
        {
            var roles= await _userservice.GetRoles();
            if (roles!=null)
            {
                var result = roles.Select(r =>new {id=r.Id, name=r.Name });
                return Ok(result);
            }
            return Forbid();
        }

        
        [Route("password")]
        [HttpGet]
        public string GeneratePassword()
        {
            var options = _userManager.Options.Password;

            int length = options.RequiredLength;

            bool nonAlphanumeric = options.RequireNonAlphanumeric;
            bool digit = options.RequireDigit;
            bool lowercase = options.RequireLowercase;
            bool uppercase = options.RequireUppercase;

            StringBuilder password = new StringBuilder();
            Random random = new Random();

            while (password.Length < length)
            {
                char c = (char)random.Next(32, 126);

                password.Append(c);

                if (char.IsDigit(c))
                    digit = false;
                else if (char.IsLower(c))
                    lowercase = false;
                else if (char.IsUpper(c))
                    uppercase = false;
                else if (!char.IsLetterOrDigit(c))
                    nonAlphanumeric = false;
            }

            if (nonAlphanumeric)
                password.Append((char)random.Next(33, 48));
            if (digit)
                password.Append((char)random.Next(48, 58));
            if (lowercase)
                password.Append((char)random.Next(97, 123));
            if (uppercase)
                password.Append((char)random.Next(65, 91));

            return password.ToString();
        }

        
        public async Task SendMail(string username,string password)
        {
            using (var smtpClient = HttpContext.RequestServices.GetRequiredService<SmtpClient>())
            {
                try
                {
                    await smtpClient.SendMailAsync(new MailMessage(
                           from: "szekacsmark@gmail.com",
                           to: "szekacs.mark@citromail.hu",
                           subject: "Test message subject",
                           body: string.Format("User Name: %0 , Password: %1", username, password)
                           ));
                }
                catch(Exception e)
                {
                    string error = e.Message;
                }
              
            }
        }
    }
}