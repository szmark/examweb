﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationService.Interfaces;
using Data;
using Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApplicationService.ViewModels;
using Microsoft.AspNetCore.Http.Internal;
using System.IO;
using ApplicationService.BusinessLogic;
using Newtonsoft.Json;

namespace ExamWeb.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ExamController : ControllerBase
    {
        IExamService _examService;
        IUserService _userService;
        IEmailService _emailService;
        public ExamController(IExamService examService,IUserService userService,IEmailService emailService)
        {
            this._examService = examService;
            this._userService = userService;
            _emailService = emailService;
        }

        [Authorize(Roles = "Student")]
        [Route("postAnswers/{id}")]
        [HttpPost]
        public async Task<ActionResult> PostAnswers(int id)
        {
            bool isAlreadyPost;
            var files = Request.Form.Files.Any() ? Request.Form.Files : new FormFileCollection();
            var model = JsonConvert.DeserializeObject<IEnumerable<QuestionViewModel>>(Request.Form["model"]);
            Exam exam = await _examService.GetExamById(id);
            if (exam.Status.Id != 2 )
            {
                return Forbid();
            }
            var examlines = await _examService.FindExamLines(id);
            try
            {
                var userid = model.FirstOrDefault().Answers.FirstOrDefault().UserId;
                isAlreadyPost = examlines.Where(x => x.UserId == userid).Count() > 0;
            }catch(Exception e)
            {
                return Ok(e);
            }
            if (isAlreadyPost)
            {
                return Forbid();
            }
            bool result = false;
            using (var transaction = _examService.GetDatabase().BeginTransaction())
            {
                foreach (var question in model)
                {
                    Question tempQuestion = await _examService.GetQuestionById(question.Id);
                    foreach (var answer in question.Answers)                      
                    {
                        Answer a = new Answer() {
                            TextAnswer=answer.TextAnswer,
                            Question= tempQuestion,
                            User=await _userService.FindUserById( answer.UserId),
                            IsCorrect=answer.IsCorrect,
                            AnswerA=answer.AnswerA,
                            AnswerB=answer.AnswerB,
                            TrueOrFalse=answer.TrueOrFalse,
                            QuestionType=answer.QuestionType
                        };                       
                        result=await _examService.CreateAnswer(a);                       
                    }
                    ExamLine tempExamLine = new ExamLine()
                    {
                        ExamId = id,
                        QuestionId = tempQuestion.Id,
                        QuestionName = tempQuestion.Name,
                        UserId = question.Answers.FirstOrDefault().UserId,
                        Value = question.Value //jelenleg max pontot kap
                                               //Value todo
                    };
                    result = await _examService.CreateExamLine(tempExamLine);
                    if (!result)
                    {
                        return Forbid();
                    }
                }

                if(result)
                {
                    transaction.Commit();                   
                }
                else
                {
                    return Forbid();
                }
            }
            //kiértékelni a vizsgát itt
            //kell egy feltétel ha csak eldőntető kérdések vannak akkor számitson pontszámot 
            result = await _examService.EvaluateExam(id, model.FirstOrDefault().Answers.FirstOrDefault().UserId);
            //var files = Request.Form.Files.Any() ? Request.Form.Files : new FormFileCollection();
            var message = new Message(new string[] { "szekacsmark@gmail.com" }, "Vizsga beküldés", "Kedves Hallgató!\n Válaszai sikeresen beküldésre kerültek.", files);
            await _emailService.SendEmailAsync(message);
            return Ok();
        }

        [Authorize(Roles = "Teacher")]
        // update exam 
        [Route("updateExam")]
        [HttpPut]
        public async Task<ActionResult> UpdateExam([FromBody] ExamViewModel model)
        {
            bool result = false;
            if(model.Status.Id!=1)
            {
                return Forbid();
            }
            using (var transaction = _examService.GetDatabase().BeginTransaction())
            {
                //Exam exam = await _examService.GetExamById(model.Id);
                Exam exam = new Exam
                {
                    Id = model.Id,
                    Title = model.Title,
                    Description = model.Description,
                    FromDate = model.FromDate,
                    ToDate = model.ToDate,
                    Status = await _examService.GetExamStatusById(model.Status.Id),
                    CreatedById = model.User.Id,
                    SubjectId = model.Subject.Id,
                    EvaluateType=model.EvaluateType
                };
                //UpdateHeader
                /*result=await _examService.UpdateExam(exam);
                if(!result)
                {
                    return Forbid();
                }*/
                //update usergroupToExam
                List<UserGroupToExam> tempGroupToExam;
                if (model.UserGroup != null)
                {
                    tempGroupToExam = model.UserGroup.Select(x => new UserGroupToExam() { ExamId = exam.Id, GroupId = x.Id }).ToList();
                    result=await _examService.UpdateGroupToExams(exam, tempGroupToExam);
                    if(!result)
                    {
                        return Forbid();
                    }
                }               
                result=await _examService.DeleteExameLines(exam.Id);            
                //add new lines 
                if (model.Questions != null)
                {
                    foreach (var question in model.Questions)
                    {
                        Question tempQuestion = new Question() { Name = question.Name };
                        result = await _examService.CreateQuestion(tempQuestion);
                        if (result)
                        {
                            //insert examLine
                            ExamLine tempExamLine = new ExamLine()
                            {
                                QuestionId = tempQuestion.Id,
                                ExamId = exam.Id,
                                QuestionName = question.Name,
                                User = null,
                                Value = question.Value
                            };
                            result = await _examService.CreateExamLine(tempExamLine);
                            if (question.Answers != null)
                            {
                                foreach (var answer in question.Answers)
                                {
                                    Answer tempAnswer = new Answer()
                                    {
                                        TextAnswer = answer.TextAnswer,
                                        TrueOrFalse = answer.TrueOrFalse,
                                        AnswerA = answer.AnswerA,
                                        AnswerB = answer.AnswerB,
                                        IsCorrect = answer.IsCorrect,
                                        User = null, // await _userService.FindUserById(answer.UserId),
                                        Question = await _examService.GetQuestionById(tempQuestion.Id),
                                        QuestionType=answer.QuestionType
                                    };
                                    //insert answer
                                    result = await _examService.CreateAnswer(tempAnswer);
                                }
                            }
                        }
                    }
                }
                if(result)
                {
                    transaction.Commit();
                }
                return Ok(exam);
            }
        }

        [Authorize(Roles = "Teacher")]
        [Route("add")]
        [HttpPost]
        public async Task<ActionResult> InsertExam([FromBody] ExamViewModel model)
        {            
            bool result=false;
            using (var transaction = _examService.GetDatabase().BeginTransaction())
            {
                Exam exam = new Exam()
                {
                    Id = model.Id,
                    Title = model.Title,
                    Description = model.Description,
                    FromDate = model.FromDate,
                    ToDate = model.ToDate,
                    Status = await _examService.GetExamStatusById(model.Status.Id),
                    CreatedById = model.User.Id,
                    SubjectId = model.Subject.Id,
                    EvaluateType=model.EvaluateType
                };
                result = await _examService.CreateExam(exam);
                if (!result)
                {
                    return Forbid();
                }
                List<UserGroupToExam> tempGroupToExam;
                if (model.UserGroup != null)
                {
                    tempGroupToExam = model.UserGroup.Select(x => new UserGroupToExam() { ExamId = exam.Id, GroupId = x.Id }).ToList();
                    foreach (var groupToexam in tempGroupToExam)
                    {
                        await _examService.AddGroupsToExam(groupToexam);
                    }
                }
                if (model.Questions != null)
                {
                    foreach (var question in model.Questions)
                    {
                        Question tempQuestion = new Question() { Name = question.Name };
                        result = await _examService.CreateQuestion(tempQuestion);
                        if (result)
                        {
                            //insert examLine
                            ExamLine tempExamLine = new ExamLine()
                            {
                                QuestionId = tempQuestion.Id,
                                ExamId = exam.Id,
                                QuestionName = question.Name,
                                User = null,
                                Value = question.Value
                            };
                            result = await _examService.CreateExamLine(tempExamLine);
                            if (question.Answers != null)
                            {
                                foreach (var answer in question.Answers)
                                {
                                    Answer tempAnswer = new Answer()
                                    {
                                        TextAnswer = answer.TextAnswer,
                                        TrueOrFalse = answer.TrueOrFalse,
                                        AnswerA = answer.AnswerA,
                                        AnswerB = answer.AnswerB,
                                        IsCorrect = answer.IsCorrect,
                                        User = null, // await _userService.FindUserById(answer.UserId),
                                        Question = await _examService.GetQuestionById(tempQuestion.Id),
                                        QuestionType=answer.QuestionType
                                    };
                                    //insert answer
                                    result = await _examService.CreateAnswer(tempAnswer);
                                }
                            }
                        }
                    }
                }
                
                if (result)
                {
                    transaction.Commit();
                    return Ok(exam.Id);
                }
                else
                {
                    return Forbid();
                }
            }
        }

        [Authorize(Roles = "Teacher")]
        [Route("self/exams/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetExamsByTeacher(string id)
        {
            List<Exam> exams = (List < Exam > )await  _examService.FindExamsByTeacher(id);
            if (exams!=null)
            {                                 
                return Ok(exams.Select(x=>new ExamViewModel() {
                    Id = x.Id,
                    Title = x.Title,
                    Description = x.Description,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    Subject = new SubjectViewModel() { Id = x.SubjectId, Name = x.Subject.Name },
                    User = new UserViewModel() { Id = x.CreatedById, Email = x.Createdby.Email },
                    Status = new ExamStatusViewModel() { Id = x.Status.Id, Name = x.Status.Name },
                    UserGroup = x.UserGroupToExam!=null ? x.UserGroupToExam.Select(y => new UserGroupViewModel() { Id = y.UserGroup.Id, Name = y.UserGroup.Name }).ToArray() :null,
                    Questions =x.ExamLines!=null ? x.ExamLines.Select(y => new QuestionViewModel()
                    {
                        Id = y.Question.Id,
                        Name = y.Question.Name,
                        Value = y.Value,
                        Answers = y.Question.Answers.Select(z => new AnswerViewModel()
                                { Id = z.Id, AnswerA = z.AnswerA, AnswerB = z.AnswerB, TextAnswer = z.TextAnswer, TrueOrFalse = z.TrueOrFalse, IsCorrect = z.IsCorrect, UserId =z.User!=null ? z.User.Id:null}).ToArray()
                    }).ToArray() :null                                
                }));
            }
            else
            {
                return Forbid();
            }
            
        }

        [Authorize(Roles = "Student")]
        [Route("access/exams/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetExamsByStudent(string id)
        {
            List<Exam> exams = (List<Exam>)await _examService.FindExamsByStudent(id);
            if (exams != null)
            {
                return Ok(exams.Select(x => new ExamViewModel()
                {
                    Id = x.Id,
                    Title = x.Title,
                    Description = x.Description,
                    FromDate = x.FromDate,
                    ToDate = x.ToDate,
                    Subject = new SubjectViewModel() { Id = x.SubjectId, Name = x.Subject.Name },
                    User = new UserViewModel() { Id = x.CreatedById, Email = x.Createdby.Email },
                    Status = new ExamStatusViewModel() { Id = x.Status.Id, Name = x.Status.Name },
                    UserGroup = x.UserGroupToExam != null ? x.UserGroupToExam.Select(y => new UserGroupViewModel() { Id = y.UserGroup.Id, Name = y.UserGroup.Name }).ToArray() : null,
                    Questions = x.ExamLines != null ? x.ExamLines.Select(y => new QuestionViewModel()
                    {
                        Id = y.Question.Id,
                        Name = y.Question.Name,
                        Value = y.Value,
                        Answers = y.Question.Answers.Select(z => new AnswerViewModel()
                        { Id = z.Id, AnswerA = z.AnswerA, AnswerB = z.AnswerB, TextAnswer = z.TextAnswer, TrueOrFalse = z.TrueOrFalse, IsCorrect = z.IsCorrect, UserId = z.User != null ? z.User.Id : null }).ToArray()
                    }).ToArray() : null
                }));
            }
            else
            {
                return Forbid();
            }
        }

        [Route("exams/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetExamById(int id)
        {
            var exam = await _examService.GetExamById(id);
            if(exam!=null)
            {
                return Ok(new ExamViewModel() {
                    Id = exam.Id,
                    Title = exam.Title,
                    Description = exam.Description,
                    FromDate = exam.FromDate,
                    ToDate = exam.ToDate,
                    Subject = new SubjectViewModel() { Id = exam.SubjectId, Name = exam.Subject.Name },
                    User = new UserViewModel() { Id = exam.CreatedById, Email = exam.Createdby.Email },
                    Status = new ExamStatusViewModel() { Id = exam.Status.Id, Name = exam.Status.Name },
                    EvaluateType=exam.EvaluateType,
                    UserGroup = exam.UserGroupToExam != null ? exam.UserGroupToExam.Select(y => new UserGroupViewModel() { Id = y.UserGroup.Id, Name = y.UserGroup.Name }).ToArray() : null,
                    Questions = exam.ExamLines != null ? exam.ExamLines.Where(l=>l.User==null).Select(y => new QuestionViewModel()
                    {
                        Id = y.Question.Id,
                        Name = y.Question.Name,
                        Value = y.Value,
                        Answers = y.Question.Answers.Where(q=>q.User==null).Select(z => new AnswerViewModel()
                        { Id = z.Id,QuestionType=z.QuestionType, AnswerA = z.AnswerA, AnswerB = z.AnswerB, TextAnswer = z.TextAnswer, TrueOrFalse = z.TrueOrFalse, IsCorrect = z.IsCorrect, UserId =z.User!=null? z.User.Id:null }).ToArray()
                    }).ToArray() : null                    
                });
            }
            else
            {
                return Forbid();
            }
        }
       
        [Route("subjects")]
        [HttpGet]
        public async Task<ActionResult> GetAllSubjects()
        {
            var subjects = await  _examService.GetAllSubject();
            if (subjects!=null)
            {
                return Ok(subjects.Select(s=>new SubjectViewModel {  Id=s.Id, Name=s.Name}));
            }
            else
            {
                return Forbid();
            }
        }
        
        [Route("subjects/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetSubjectById(int id)
        {
            var subject = await  _examService.GetSubjectById(id);
            if(subject!=null)
            {
                return Ok( new SubjectViewModel { Id = subject.Id, Name = subject.Name });
            }
            else
            {
                return Forbid();
            }
        }
        
        [Route("groups")]
        [HttpGet]
        public async Task<ActionResult> GetAllGroups()
        {
            var groups= await _examService.GetAllGroups();
            if (groups != null)
            {
                return Ok(groups.Select(s => new UserGroupViewModel { Id = s.Id, Name = s.Name }));
            }
            else
            {
                return Forbid();
            }
        }
        
        [Route("examsStat/{db}/{direction}")]
        [HttpGet]
        public async Task<ActionResult> ExamsToStat(int db,bool direction)
        {
            var res = await _examService.GetDifficultExams(db, direction);
            if(res!=null)
            {
                return Ok(res);
            }
            else
            {
                return Forbid();
            }
        }

        [Authorize(Roles = "Teacher")]
        [Route("questionsStat/{db}/{direction}")]
        [HttpGet]
        public async Task<ActionResult> QuestionsToStat(int db, bool direction)
        {
            var res = await _examService.GetDifficultQuestions(db, direction);
            if (res != null)
            {
                return Ok(res);
            }
            else
            {
                return Forbid();
            }
        }

        [Authorize(Roles = "Teacher")]
        [Route("usersStat/{db}/{direction}")]
        [HttpGet]
        public async Task<ActionResult> UsersToStat(int db, bool direction)
        {
            var users = await  _userService.GetAllUsers();
            var res = await _examService.GetSuccessfulUsers(db, direction,users);
            if (res != null)
            {
                return Ok(res);
            }
            else
            {
                return Forbid();
            }
        }

        [AllowAnonymous]
        [Route("TestEmail")]
        [HttpPost]
        public async Task<ActionResult> EmailWithAttachmentTest()
        {
            var files = Request.Form.Files.Any() ? Request.Form.Files : new FormFileCollection();

            var message = new Message(new string[] { "codemazetest@mailinator.com" }, "Test mail with Attachments", "This is the content from our mail with attachments.", files);
            await _emailService.SendEmailAsync(message);
            return Ok();
        }

        [Authorize(Roles = "Teacher")]
        [Route("releaseExam/{id}")]
        [HttpPut]
        public async Task<ActionResult> ReleaseExam(int id)
        {
            Exam exam = await _examService.GetExamById(id);
            if(exam.Status.Id==1) //ha Open
            {
                exam.Status =await  _examService.GetExamStatusById(2);
                bool result=await _examService.UpdateExam(exam);
                if (result)
                {
                    return Ok(exam);
                }
                else
                { return Forbid(); }
            }
            else
            {
                return Forbid();
            }
        }

        [Authorize(Roles = "Teacher")]
        [Route("closeExam/{id}")]
        [HttpPut]
        public async Task<ActionResult> CloseExam(int id)
        {
            Exam exam = await _examService.GetExamById(id);
            if (exam.Status.Id <3) //ha még nincs lezárva
            {
                IEnumerable<ExamLine> examLines = await _examService.FindExamLines(id);
                if(examLines.ToList().Where(x=>x.Value==null).Count()>0 )
                {
                    return Forbid();
                }
                exam.Status = await _examService.GetExamStatusById(3);
                bool result = await _examService.UpdateExam(exam);
                if (result)
                {
                    return Ok(exam);
                }
                else
                { return Forbid(); }
            }
            else
            {
                return Forbid();
            }
        }

        [Authorize(Roles = "Teacher")]
        [Route("correctExam/{id}")]
        [HttpPost]
        public async Task<ActionResult> CorrectExam(int id)
        {
            Exam exam = await _examService.GetExamById(id);
            if(exam.Status.Id!=3)
            {
                return Forbid();
            }
            var model = JsonConvert.DeserializeObject<IEnumerable<QuestionViewModel>>(Request.Form["model"]);
            bool result = false;
            foreach (var question in model)
            {
                QuestionType questionType =(QuestionType) question.Answers[0].QuestionType;
                if (questionType == QuestionType.Text)
                {
                    Question tempQuestion = await _examService.GetQuestionById(question.Id);
                    ExamLine tempExamLine = new ExamLine()
                    {
                        ExamId = id,
                        QuestionId = tempQuestion.Id,
                        QuestionName = tempQuestion.Name,
                        UserId = question.Answers.FirstOrDefault().UserId,
                        Value = question.Value
                    };
                    result = await _examService.UpdateExamLine(tempExamLine);
                    if (!result)
                    {
                        return Forbid();
                    }
                }
            }
            var files = Request.Form.Files.Any() ? Request.Form.Files : new FormFileCollection();
            var message = new Message(new string[] { "szekacs.mark@citromail.hu" }, "Test mail with Attachments", "Kedves Hallgató!\n Csatolva küldjök az eredményét a  vizsgát illetően.", files);
            await _emailService.SendEmailAsync(message);
            return Ok();
        }

        [Authorize(Roles = "Teacher")]
        [Route("examResult/{id}/students")]
        [HttpGet]
        public async Task<ActionResult> GetUsersResultsByExam(int id)
        {
            var users = await _userService.GetAllUsers();
            IEnumerable<UserStatViewModel> model = await _examService.GetUsersResultsByExamId(id, users);
            if(model!=null)
            {
                return Ok(model);
            }
            else
            {
                return Forbid();
            }
        }

        [Authorize(Roles = "Teacher")]
        [Route("examResult/{examid}/students/{userid}")]
        [HttpGet]
        public async Task<ActionResult> GetResultbyExamAndUser(int examid,string userid)
        {
            AnswerLogic al = new AnswerLogic();
            ExamLineLogic el = new ExamLineLogic();
            var examlines =await  _examService.FindExamLines(examid);
            //var solutions = examlines.Where(x => x.User == null).ToList();
            var usersolutions = examlines.Where(x => x.UserId == userid).ToList();
            QuestionViewModel[] model = usersolutions != null ? usersolutions.Select(y => new QuestionViewModel()
            {
                Id = y.Question.Id,
                Name = y.Question.Name,
                Value = y.Value,
                //MaxValue=examlines.Where(g=>g.QuestionId==y.QuestionId || g.User==null).FirstOrDefault().Value,
                MaxValue = el.GetAchiveAbleValue(examlines.ToList(), y.QuestionId, y.ExamId),
                Answers = y.Question.Answers.Where(q => q.UserId == userid).Select(z => new AnswerViewModel()
                { Id = z.Id, QuestionType = z.QuestionType, AnswerA = z.AnswerA, AnswerB = z.AnswerB, TextAnswer = z.TextAnswer, TrueOrFalse = z.TrueOrFalse, IsCorrect = z.IsCorrect, UserId = z.UserId  }).ToArray()
            }).ToArray() : null;

            if(model!=null)
            {
                return Ok(model);
            }
            else
            {
                return Forbid();
            }
        }


    }
}