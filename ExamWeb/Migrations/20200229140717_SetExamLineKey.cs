﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ExamWeb.Migrations
{
    public partial class SetExamLineKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ExamLines",
                table: "ExamLines");

            migrationBuilder.DropColumn(
                name: "LineNo",
                table: "ExamLines");

            migrationBuilder.AddColumn<int>(
                name: "LineId",
                table: "ExamLines",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ExamLines",
                table: "ExamLines",
                column: "LineId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamLines_ExamId",
                table: "ExamLines",
                column: "ExamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ExamLines",
                table: "ExamLines");

            migrationBuilder.DropIndex(
                name: "IX_ExamLines_ExamId",
                table: "ExamLines");

            migrationBuilder.DropColumn(
                name: "LineId",
                table: "ExamLines");

            migrationBuilder.AddColumn<int>(
                name: "LineNo",
                table: "ExamLines",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ExamLines",
                table: "ExamLines",
                columns: new[] { "ExamId", "QuestionId" });
        }
    }
}
