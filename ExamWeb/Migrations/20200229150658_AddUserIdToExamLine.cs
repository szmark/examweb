﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ExamWeb.Migrations
{
    public partial class AddUserIdToExamLine : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "ExamLines",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ExamLines_UserId",
                table: "ExamLines",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_ExamLines_AspNetUsers_UserId",
                table: "ExamLines",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExamLines_AspNetUsers_UserId",
                table: "ExamLines");

            migrationBuilder.DropIndex(
                name: "IX_ExamLines_UserId",
                table: "ExamLines");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "ExamLines");
        }
    }
}
