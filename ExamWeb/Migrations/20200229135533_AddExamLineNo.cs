﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ExamWeb.Migrations
{
    public partial class AddExamLineNo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LineNo",
                table: "ExamLines",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LineNo",
                table: "ExamLines");
        }
    }
}
