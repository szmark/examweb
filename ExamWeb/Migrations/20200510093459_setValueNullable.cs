﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ExamWeb.Migrations
{
    public partial class setValueNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Value",
                table: "ExamLines",
                nullable: true,
                oldClrType: typeof(double));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Value",
                table: "ExamLines",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);
        }
    }
}
