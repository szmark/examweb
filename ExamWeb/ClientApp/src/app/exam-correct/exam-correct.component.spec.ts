import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamCorrectComponent } from './exam-correct.component';

describe('ExamCorrectComponent', () => {
  let component: ExamCorrectComponent;
  let fixture: ComponentFixture<ExamCorrectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamCorrectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamCorrectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
