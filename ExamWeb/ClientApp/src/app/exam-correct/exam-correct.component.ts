import { Component, OnInit } from '@angular/core';
import { Exam } from '@/_models/exam';
import { ExamService } from '@/_services/exam.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Question } from '@/_models/question';

@Component({
  selector: 'app-exam-correct',
  templateUrl: './exam-correct.component.html',
  styleUrls: ['./exam-correct.component.css']
})
export class ExamCorrectComponent implements OnInit {
  exam:Exam;
  isModifiable:boolean;
  questions:Question[];
  toPdfVisible=true;

  constructor(private examService: ExamService,private route: ActivatedRoute,private router:Router) { }

  ngOnInit() {
    this.isModifiable=true;
    this.route.params.subscribe(params=> {
      this.examService.getExamById(this.route.snapshot.params['id']).subscribe(res => {
        console.log(params['id']+' paramsid');
        this.exam = res;
        if(this.exam.status.id <2) {
          this.router.navigate(['']);
        }
        console.log(this.exam);
      })
      this.examService.getResultByUserAndExam(this.route.snapshot.params['id'],this.route.snapshot.params['sid']).subscribe(res => {
        console.log(params['id']+' paramsid');
        console.log(params['sid']+' paramsid2');
        this.questions=res;
        })
      });
    }
}
