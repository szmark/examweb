﻿import { Component } from '@angular/core';
import { first } from 'rxjs/operators';

import { User, Role } from '@/_models';
import { UserService, AuthenticationService } from '@/_services';
import { Input, Output, EventEmitter } from '@angular/core';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    currentUser: User;
    userFromApi: User;

    constructor(
        private userService: UserService,
        private authenticationService: AuthenticationService
    ) {
        this.currentUser = this.authenticationService.currentUserValue;
    }

    ngOnInit() {
        this.userService.getById(this.currentUser.id).pipe(first()).subscribe(user => {
            this.userFromApi = user;
            console.log(this.userFromApi.role);
        });
    }

    get IsStudent() {
      console.log('userfromapi role:'+this.userFromApi.role)
     return this.userFromApi && this.userFromApi.role===Role.Student;
   }
   get IsTeacher() {
    return this.userFromApi && this.userFromApi.role===Role.Teacher;
  }
}
