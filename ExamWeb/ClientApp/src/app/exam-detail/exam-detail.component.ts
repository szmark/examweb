import { Component, OnInit } from '@angular/core';
import { Exam } from '@/_models/exam';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ExamService } from '@/_services/exam.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from '@/_models/subject';
import { of } from 'rxjs';
import { DatePipe } from '@angular/common';
import { Question } from '@/_models/question';
import { QuestionType } from '@/_models/question-type.enum';
import { Answer } from '@/_models/answer';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { UserGroup } from '@/_models/user-group';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '@/_services';
import { User } from '@/_models';
import { ExamStatus } from '@/_models/exam-status';
//import {MatTabsModule} from '@angular/material/tabs';

@Component({
  selector: 'app-exam-detail',
  templateUrl: './exam-detail.component.html',
  styleUrls: ['./exam-detail.component.css']
})
export class ExamDetailComponent implements OnInit {
  tabid:number;
  isSubmitted = false;
  exam:Exam;
  examHeaderForm: FormGroup;
  loading = false;
  returnUrl: string;
  error = '';
  subjects:Subject[];
  date:string;
  isModifiable:boolean =true;
  groups: any[];
  allGroup:UserGroup[];
  currentUser: User;
  toPdfVisible=false;

  constructor(private examService: ExamService,private fb: FormBuilder,private route: ActivatedRoute,private router:Router, public datepipe: DatePipe,private authenticationService: AuthenticationService) {
    this.currentUser = this.authenticationService.currentUserValue;
    /*if(this.route.snapshot.params['id']==undefined)
    {
      this.exam=new Exam();
    }
    else
    {
      this.route.params.subscribe(params=> {
        this.examService.getExamById(this.route.snapshot.params['id']).subscribe(res => {
          console.log(params['id']+' paramsid');
          this.exam = res;
          console.log(this.exam);
      })});
    }*/

   }

  ngOnInit() {

    this.examHeaderForm = this.fb.group({
      title: ['', [Validators.required]],
      desc:[''],
      fromDate:['',],
      toDate: ['',],
      subjectControl:[''],
      evaluateType:['']

    });
    this.examService.getAllUserGroups().subscribe(res=> {
      this.allGroup=res;
      console.log(this.allGroup);
      this.groups=new Array<any>();
      this.allGroup.forEach(element => {
        this.groups.push({id:element.id,name:element.name, ischecked:false});
      });
    });
    if(this.route.snapshot.params['id']==undefined)
    {
      this.exam=new Exam();
      this.exam.questions=new Array<Question>();
      console.log(this.exam);
    }
    else
    {
      this.route.params.subscribe(params=> {
        this.examService.getExamById(this.route.snapshot.params['id']).subscribe(res => {
          console.log(params['id']+' paramsid');
          this.exam = res;
          console.log(this.exam);
          if(this.exam.status.id>1) {
            this.router.navigate(['']);
          }
          this.exam.userGroup.forEach(element => {
            var g=this.groups.find(x=>x.id==element.id);
            let index=this.groups.indexOf(g);
            this.groups[index].ischecked=true;
            console.log(index+" : "+" id: "+ this.groups[index].id+" " +this.groups[index].ischecked);
          });
          this.date=this.datepipe.transform(this.exam.fromDate,'dd/MM/yyyy');
          if( this.exam.subject!=undefined)
          {
            this.headerControl.subjectControl.setValue(this.exam.subject.id);
          }
      })});
    }

    //this.examService.getAllSubjects().subscribe(res=>this.subjects=res);
    of(this.examService.getAllSubjects().subscribe(res=>{
      this.subjects = res;
      console.log (this.subjects);
    }));
  }

  get headerControl() { return this.examHeaderForm.controls; }

  showHeaderDetails() {
    this.tabid=1;
    console.log(this.tabid);
  }
  showUserGroups() {
    this.tabid=2;
    console.log(this.tabid);
  }
  showQuestions() {
    this.tabid=3;
    console.log(this.tabid);
  }

  get IsHeaderTab() {
    return this.tabid==1;
  }

  get IsAccessTab() {
    return this.tabid==2;
  }

  get IsQuestionsTab() {
    return this.tabid==3;
  }

  saveExam()
  {
    //console.log( this.exam);
    //set status
    if(this.exam.id==undefined)
    {
      of(this.examService.getSubjectById(this.headerControl.subjectControl.value).subscribe(
        res=>{
          this.exam.subject=res;
           console.log(res);
        }
      ));
      //this.exam.subject=this.headerControl.subjectControl.value;
      console.log(this.exam.subject.id);
      //set creator
      console.log("creator: "+this.currentUser);
      this.exam.user=this.currentUser;
      //set status default Open
      this.exam.status=new ExamStatus();
      this.exam.status.id=1;
      this.exam.status.name="Open";
      //set user group
      if(this.groups.filter(x=>x.ischecked==true).length>0){
        this.exam.userGroup=new Array<UserGroup>();
        this.groups.forEach(element => {
          if(element.ischecked) {
            this.exam.userGroup.push(element);
          }
        });
    }
      console.log('new exam added.');
      console.log( this.exam);
      this.loading = true;
      this.examService.addExam(this.exam)
      .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate(['']);
                },
                error => {
                    this.error = error;
                    this.loading = false;
                });
    }
    else
    {
      console.log('exam updated: '+this.exam.id);
      this.loading = true;
      this.exam.user=this.currentUser;
      this.examService.updateExam(this.exam)
      .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate(['']);
                },
                error => {
                    this.error = error;
                    console.log("error: "+error)
                    this.loading = false;
                });
    }
  }

}
