import { Component, OnInit } from '@angular/core';
import { ExamService } from '@/_services/exam.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Exam } from '@/_models/exam';

@Component({
  selector: 'app-exam-take',
  templateUrl: './exam-take.component.html',
  styleUrls: ['./exam-take.component.css']
})
export class ExamTakeComponent implements OnInit {
  exam:Exam;
  isModifiable:boolean;
  toPdfVisible=false;

  constructor(private examService: ExamService,private route: ActivatedRoute,private router:Router) { }

  ngOnInit() {
    this.isModifiable=false;
    this.route.params.subscribe(params=> {
      this.examService.getExamById(this.route.snapshot.params['id']).subscribe(res => {
        console.log(params['id']+' paramsid');
        this.exam = res;
        if(this.exam.status.id !=2) {
          this.router.navigate(['']);
        }
        console.log(this.exam);
      })
    });
  }

}
