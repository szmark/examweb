import { Component, OnInit } from '@angular/core';
import { ExamService } from '@/_services/exam.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Exam } from '@/_models/exam';
import { UserStat } from '@/_models/user-stat';

@Component({
  selector: 'app-exam-students',
  templateUrl: './exam-students.component.html',
  styleUrls: ['./exam-students.component.css']
})
export class ExamStudentsComponent implements OnInit {
  results:UserStat[];
  examid:number;

  constructor(private examService: ExamService,private route: ActivatedRoute,private router:Router) { }

  ngOnInit() {
    this.route.params.subscribe(params=> {
      this.examService.getUserResultByExam(this.route.snapshot.params['id']).subscribe(res => {
        this.results = res;
        this.examid=this.route.snapshot.params['id'];
      })
    });
  }

}
