﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '@/_models';
import { UserService } from '@/_services';
import { ColDef, GridApi, ColumnApi } from 'ag-grid-community';
import { Router } from '@angular/router';

@Component({ templateUrl: 'admin.component.html' })
export class AdminComponent implements OnInit {
    users: User[] = [];
    columnDefs = [
      {headerName: 'Id', field: 'id'},
      {headerName: 'Username', field: 'username' },
      {headerName: 'Role', field: 'role' }
    ];
    private api: GridApi;
    private columnApi: ColumnApi;
    private userToBeEditedFromParent : any ;


    constructor(private userService: UserService,private router:Router) { }

    ngOnInit() {
        this.userService.getAll().pipe(first()).subscribe(users => {
            this.users = users;
        });
    }

    // one grid initialisation, grap the APIs and auto resize the columns to fit the available space
onGridReady(params): void {
  this.api = params.api;
  this.columnApi = params.columnApi;
  this.api.sizeColumnsToFit();
}


status: any;

//Update user
editUser(){

if(this.api.getSelectedRows().length == 0){
  return;
}
var row  = this.api.getSelectedRows();
console.log(row[0].id);
this.router.navigate(['/admin/user/',row[0].id]);
/*this.userService.editUser(row[0])
.subscribe( data => {
  console.log('Edit call output' );
  console.log(data);
  this.router.navigate(['users']);
});*/

}

createUser() {
  this.router.navigate(['admin/addUser']);
}

//Get all rows
getRowData() {
var rowData = [];
this.api.forEachNode(function(node) {
  rowData.push(node.data);
});
console.log("Row Data:");
console.log(rowData);
}

//Delete user
deleteUser() {

  var selectedRows = this.api.getSelectedRows();

  if(selectedRows.length == 0){
    return;
  }
  this.userService.deleteUser(selectedRows[0].id).subscribe(data => this.status = data );
  console.log("Deletion status: "+ this.status);

  this.ngOnInit();
  this.api.redrawRows(null);

}

selectedrow: User;
//Get updated row
onSelectionChanged() {
  var selectedRows = this.api.getSelectedRows();
  //set selectedrow
  this.selectedrow= new User();
  this.selectedrow.id=selectedRows[0].id;
  this.selectedrow.username=selectedRows[0].username;
  this.selectedrow.role=selectedRows[0].role;
  console.log(this.selectedrow.id+' TEST');

  this.userToBeEditedFromParent = selectedRows;
  console.log(this.userToBeEditedFromParent);

  var selectedRowsString = "";
  selectedRows.forEach(function(selectedRow, index) {
    if (index > 5) {
      return;
    }
    if (index !== 0) {
      selectedRowsString += ", ";
    }
    selectedRowsString += selectedRow.id;
  });
  if (selectedRows.length >= 5) {
    selectedRowsString += " - and " + (selectedRows.length - 5) + " others";
  }
}

//Get edited row
newData = [];
onCellEditingStopped(e) {
 //console.log(e.data);

 this.api.forEachNode(node=>{
     if(!node.data.id)
         this.newData.push(node.data)
 });
 console.log("On editing stopped");
 console.log(this.newData);
}

//Get updated row
onrowValueChanged(row){
  console.log("onrowValueChanged: ");
  console.log("onrowValueChanged: "+row);
}
}
