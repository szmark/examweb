﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { AdminComponent } from './admin';
import { LoginComponent } from './login';
import { AuthGuard } from './_guards';
import { Role } from './_models';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { ExamDetailComponent } from './exam-detail/exam-detail.component';
import { ExamTakeComponent} from './exam-take/exam-take.component';
import {ChartsComponent} from './charts/charts.component';
import { RegistComponent} from './regist/regist.component';
import { ExamStudentsComponent} from './exam-students/exam-students.component';
import {ExamCorrectComponent} from './exam-correct/exam-correct.component';

const appRoutes: Routes = [
    {
        path: '',
        component: HomeComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'admin',
        component: AdminComponent,
        canActivate: [AuthGuard],
        data: { roles: [Role.Admin] }
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
      path: 'regist',
      component: RegistComponent
  },
    {
    path: 'admin/user/:id',
    component: UserDetailComponent
    },
    {
    path: 'admin/addUser',
    component: UserDetailComponent
    },
    {
      path:'exam/:id',
      component:ExamDetailComponent
    },
    {
      path:'addExam',
      component:ExamDetailComponent
    },
    {
      path:'exam/:id/take',
      component:ExamTakeComponent
    },
    {
      path:'charts',
      component: ChartsComponent
    },
    {
      path:'exam/:id/students',
      component: ExamStudentsComponent
    },
    {
      path:'exam/:id/students/:sid',
      component:ExamCorrectComponent
    },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
