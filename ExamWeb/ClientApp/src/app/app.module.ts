﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
//import {MatTabsModule} from '@angular/material/tabs';



// used to create fake backend
import { fakeBackendProvider } from './_helpers';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { HomeComponent } from './home';
import { AdminComponent } from './admin';
import { LoginComponent } from './login';
import { AgGridModule } from 'ag-grid-angular';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { ExamListComponent } from './exam-list/exam-list.component';
import { ExamDetailComponent } from './exam-detail/exam-detail.component';
import { DatePipe } from '@angular/common';
import { QuestionListComponent } from './question-list/question-list.component';
import { ExamTakeComponent } from './exam-take/exam-take.component';
import { ChartsComponent } from './charts/charts.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";;
import { RegistComponent } from './regist/regist.component'
;
import { ExamStudentsComponent } from './exam-students/exam-students.component'
;
import { ExamCorrectComponent } from './exam-correct/exam-correct.component';
@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        routing,
        AgGridModule.withComponents([HomeComponent]),
        FormsModule,
        FontAwesomeModule,
        MDBBootstrapModule.forRoot(),
        BrowserAnimationsModule
        ],

    declarations: [
        AppComponent,
        HomeComponent,
        AdminComponent,
        LoginComponent,
        UserDetailComponent,
        ExamListComponent,
        ExamDetailComponent,
        ExamTakeComponent,
        QuestionListComponent,
        ChartsComponent ,
        RegistComponent ,
        ExamStudentsComponent ,
        ExamCorrectComponent   ],

    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        DatePipe

        // provider used to create fake backend
        //fakeBackendProvider
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
