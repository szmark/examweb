import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ExamService } from '@/_services/exam.service';
import { ExamStat } from '@/_models/exam-stat';
import { first } from 'rxjs/operators';
import { of } from 'rxjs';
import { UserStat } from '@/_models/user-stat';
import { QuestionStat } from '@/_models/question-stat';


@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {

  public chartType: string = 'bar';
  examStat:Array<ExamStat>;
  userStat:Array<UserStat>;
  questionStat:Array<QuestionStat>;
  tempData:any[];

  public userChartDatasets:Array<any>;
  public questionChartDatasets:Array<any>;
  public userChartLabels:Array<any>;
  public questionChartLabels:Array<any>;

  public chartDatasets: Array<any>;
  /*= [
    { data: [65.1, 59, 80, 81, 56, 55, 40], label: 'Most difficult questions %' }
  ];*/

  public chartLabels: Array<any>;
  // = ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'];

  public chartColors: Array<any> = [
    {
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true,
      scales: {
        xAxes: [{
          stacked: true
          }],
        yAxes: [
        {
          stacked: true
        }
      ]
    }
  };

  constructor(private examService: ExamService) { }

  ngOnInit() {
    this.intExamDataToChart();
    this.intUserDataToChart();
    this.intQuestionDataToChart();

  }

  intUserDataToChart()
  {
      of(this.examService.getUsersToStat(2,true).pipe(first()).subscribe(x=> {
        this.userStat=x;
      if (this.userStat!=undefined && this.userStat !=null) {
        this.tempData=new Array();
        this.userStat.forEach(element => {
          this.tempData.push(element.avgPercent*100);
        });
        this.userChartDatasets=[{ data:this.tempData,label: 'Most Successful users %'}];
        this.userChartLabels=this.userStat.map(y=>y.name);
      }
    }));
  }
  intQuestionDataToChart()
  {
      of(this.examService.getQuestionsToStat(2,true).pipe(first()).subscribe(x=> {
        this.questionStat=x;
      if (this.questionStat!=undefined && this.questionStat !=null) {
        this.tempData=new Array();
        this.questionStat.forEach(element => {
          this.tempData.push(element.avgPercent*100);
        });
        this.questionChartDatasets=[{ data:this.tempData,label: 'Avg Result % for question'}];
        this.questionChartLabels=this.questionStat.map(y=>y.questionName);
      }
    }));
  }

  intExamDataToChart()
  {
      of(this.examService.getExamsToStat(2,true).pipe(first()).subscribe(x=> {
        this.examStat=x;
      if (this.examStat!=undefined && this.examStat !=null) {
        this.tempData=new Array();
        this.examStat.forEach(element => {
          this.tempData.push(element.avgPercent*100);
        });
        this.chartDatasets=[{ data:this.tempData,label: 'Most difficult exams %'}];
        this.chartLabels=this.examStat.map(y=>y.title);
      }
    }));
  }


  chartHovered()
  {

  }

  chartClicked()
  {

  }

}
