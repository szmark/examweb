import { Component, OnInit } from '@angular/core';
import { User, Role } from '@/_models';
import { AuthenticationService, UserService } from '@/_services';
import { first } from 'rxjs/operators';
import { GridApi, ColumnApi } from 'ag-grid-community';
import { ExamService } from '@/_services/exam.service';
import { Exam } from '@/_models/exam';
import { Router } from '@angular/router';

@Component({
  selector: 'app-exam-list',
  templateUrl: './exam-list.component.html',
  styleUrls: ['./exam-list.component.css']
})
export class ExamListComponent implements OnInit {
  status: any;
  selectedrow: Exam;
  currentUser: User;
  userFromApi: User;
  teacherExams: Exam[] = [];
  studentExams: Exam[] = [];
  error='';
  loading:boolean;

  constructor(
    private userService: UserService, private authenticationService: AuthenticationService,
    private examservice:ExamService,private router:Router) {
      this.currentUser = this.authenticationService.currentUserValue;
   }

  ngOnInit() {
    this.userService.getById(this.currentUser.id).pipe(first()).subscribe(user => {
      this.userFromApi = user;
      console.log(this.userFromApi.role);
    });
      if(this.IsTeacher)
      {
        this.examservice.getExamsByTeacher(this.currentUser.id).pipe(first()).subscribe(exams => {
          this.teacherExams = exams;
        });
      }
      else if (this.IsStudent)
      {
        this.examservice.getExamsByStudent(this.currentUser.id).pipe(first()).subscribe(exams => {
          this.studentExams = exams;
        });
      }
  }

  createExam() {
    this.router.navigate(['addExam']);
  }


  get IsStudent() {
    console.log('userfromapi role:'+this.currentUser.role)
    return this.currentUser && this.currentUser.role===Role.Student;
  }
  get IsTeacher() {
    return this.currentUser && this.currentUser.role===Role.Teacher;
  }

  releaseExam(exam:Exam) {
    this.examservice.setExamToRelease(exam.id,3)
    .pipe(first())
          .subscribe(
              data => {
                  //this.router.navigate(['']);
              },
              error => {
                  this.error = error;
                  this.loading = false;
              });
  }

  closeExam(exam:Exam) {
    this.examservice.setExamToClosed(exam.id,3)
    .pipe(first())
          .subscribe(
              data => {
                  //this.router.navigate(['']);
              },
              error => {
                  this.error = error;
                  this.loading = false;
              });
  }
}
