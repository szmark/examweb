import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService, AuthenticationService } from '@/_services';
import { first } from 'rxjs/operators';
import { User, Role } from '@/_models';

@Component({
  selector: 'app-regist',
  templateUrl: './regist.component.html',
  styleUrls: ['./regist.component.css']
})
export class RegistComponent implements OnInit {

  user: User;
  registForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';

  constructor(
    private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private UserService: UserService,
        private AuthenticationService:AuthenticationService)
  {
    // redirect to home if already logged in
    if (this.AuthenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.registForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      ConfirmPassword:['',Validators.required]
  },{
    validator: RegistComponent.MustMatch('password','ConfirmPassword')
});

  // get return url from route parameters or default to '/'
  this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
}

// convenience getter for easy access to form fields
get f() { return this.registForm.controls; }

public static MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          // return if another validator has already found an error on the matchingControl
          return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
          matchingControl.setErrors(null);
      }
  }
}

onSubmit() {
  this.submitted = true;

  // stop here if form is invalid
  if (this.registForm.invalid) {
      return;
  }

  this.loading = true;
  this.user=new User();
  this.user.username=this.f.username.value;
  this.user.password=this.f.password.value;
  this.user.role=Role.Student;
  //this.authenticationService.login(this.f.username.value, this.f.password.value)
  this.AuthenticationService.regist(this.f.username.value,this.f.password.value,Role.Student)
      .pipe(first())
      .subscribe(
          data => {
              this.AuthenticationService.login(this.f.username.value, this.f.password.value);
              this.router.navigate([this.returnUrl]);
          },
          error => {
              this.error = error;
              this.loading = false;
          });
}
}
