import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '@/_services';
import { User, Role } from '@/_models';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  roleName: any;
  userForm: FormGroup;
  isSubmitted = false;
  roles: any[];
  user: User;
  loading = false;
    submitted = false;
    returnUrl: string;
    error = '';


  constructor(private userService: UserService,private fb: FormBuilder,private route: ActivatedRoute,private router:Router) {

  }

  ngOnInit() {
    this.userForm = this.fb.group({
      roleName: ['', [Validators.required]],
      id:[''],
      username:['',[Validators.required]],

    })
    console.log("snapshotparams: "+this.route.snapshot.params['id']);
    console.log('params: '+this.route.params['id']);
    if(this.route.snapshot.params['id']==undefined)
    {
      this.user=new User();
    }
    else
    {
      this.route.params.subscribe(params=> {
        this.userService.getById(this.route.snapshot.params['id']).subscribe(res => {
          console.log(params['id']+' paramsid');
          this.user = res;
          console.log(this.user);
          if( this.user.role!=undefined)
          {
            this.f.roleName.setValue(this.user.role);
          }
      })});
    }
    of(this.userService.getRoles().subscribe(roles=>{
      this.roles = roles;
      //this.f.roleName.setValue(this.user.role);
    }));
  }

  get f() { return this.userForm.controls; }

  changeRole(e) {
    this.roleName.setValue(e.target.value, {
      onlySelf: true
    })
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.userForm.invalid) {
      console.log('userform is invalid.');
      return;
    }
    this.user.role=this.userForm.controls.roleName.value;
    if(this.user.id==undefined)
    {
      console.log('new user added.');
      this.loading = true;
      this.userService.addUser(this.user)
      .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate(['/admin/users']);
                },
                error => {
                    this.error = error;
                    this.loading = false;
                });
    }
    else
    {
      console.log('new user edited: '+this.user.id);
      this.loading = true;
      this.userService.editUser(this.user)
      .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate(['/admin/users']);
                },
                error => {
                    this.error = error;
                    this.loading = false;
                });
    }
  }
}
