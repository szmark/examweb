import { Component, OnInit, Input } from '@angular/core';
import { Question } from '@/_models/question';
import { QuestionType } from '@/_models/question-type.enum';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import icons from 'glyphicons';
import { Answer } from '@/_models/answer';
import { AuthenticationService} from '@/_services';
import { User } from '@/_models';
import { ExamService } from '@/_services/exam.service';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

import {ViewChild, ElementRef } from '@angular/core';
import * as jsPDF from 'jspdf';
import * as html2canvasWrong from "html2canvas";
import { of } from 'rxjs';

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.css']
})
export class QuestionListComponent implements OnInit {

  @Input() questions?:Question[];
  @Input() isModifiable:boolean;
  @Input() examid:number;
  @Input() ispdfVisible:boolean;
  selectedQuestion:Question;
  viewDeails:boolean;
  questionTypes=[];
  defaultType:QuestionType;
  error = '';
  toPdfVisible:boolean;
  selectedType:number;
  private QuestionTypes=QuestionType;

  currentUser:User;


  @ViewChild('details', {static: false}) details: ElementRef;

  constructor(authenticationService:AuthenticationService,private examService :ExamService, private router:Router) {  this.currentUser = authenticationService.currentUserValue; }

  ngOnInit() {
    if(this.isModifiable!=true)
    {
      this.initUserAnswers(this.currentUser.id);
      console.log(this.questions[0].answers);
    }
    this.defaultType=QuestionType.MultipleChoice;
    this.questionTypes=Object.keys(this.QuestionTypes);
    console.log(this.questionTypes);
    if(this.questions.length>0)
    {
      this.viewDeails=true;
      this.selectedQuestion=this.questions[0];
    }
    this.toPdfVisible=this.ispdfVisible;

  }

  getSelectedQuestion(q:Question)
  {
    this.viewDeails=true;
    this.selectedQuestion=q;
  }

  newQuestions()
  {
    this.selectedQuestion=new Question();
    console.log('type: '+this.selectedType);
    if(this.selectedType==0) {
      this.selectedQuestion.answers=new Array<Answer>();
      var _answer1=new Answer();
    _answer1.questionType=QuestionType.MultipleChoice;
    var _answer2=new Answer();
    _answer2.questionType=QuestionType.MultipleChoice;
    this.selectedQuestion.answers.push(_answer1);
    this.selectedQuestion.answers.push(_answer2);

    }
    if (this.selectedType==1) {
      var textAnswer=new Answer();
      textAnswer.questionType=QuestionType.Text;
      this.selectedQuestion.answers=new Array<Answer>();
      this.selectedQuestion.answers.push(textAnswer);
    } else if(this.selectedType==2) {
      var trueAnswer=new Answer();
      trueAnswer.questionType=QuestionType.TrueOrFalse;
      this.selectedQuestion.answers=new Array<Answer>();
      this.selectedQuestion.answers.push(trueAnswer);

    }
    this.questions.push(this.selectedQuestion);
    this.viewDeails=true;
    //this.questions.push(this.selectedQuestion);
  }

  deleteQuestion()
  {
    const index = this.questions.indexOf(this.selectedQuestion, 0);
    console.log(index);
    if (index > -1 && index<this.questions.length) {
      console.log("deleted question: "+index);
      this.questions.splice(index, 1);
      this.selectedQuestion=null;
      this.viewDeails=false;
      /*if(index-1<0)
      {
        this.selectedQuestion=null;
      }
      else
      {
        this.selectedQuestion=this.questions[index-1];
      }*/
    }
  }

  anyQuestionIsSelected()
  {
    if(this.selectedQuestion!=null && this.selectedQuestion!=undefined )
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  saveQuestion(question) //ez leeht nem kell
  {
    //this.questions.find(e=>e.id==this.selectedQuestion.id);
  }

  async correctExam() {
    var pdf:File;
    await this.generatePDF(false).then(value => {pdf=this.blobToFile(value,'solution.pdf');});
    var data = new FormData();
    data.append("data" , pdf,pdf.name);
    console.log(this.questions);
    data.append("model",JSON.stringify(this.questions));
    this.examService.updateSolutionPts(data,this.examid)
    .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate(['']);
                },
                error => {
                    this.error = error;
                });
  }

  addNewAnswer() {
    var _answer :Answer;
    _answer=new Answer();
    _answer.questionType=QuestionType.MultipleChoice;
    if(this.selectedQuestion.answers==undefined)
    {
      this.selectedQuestion.answers=new Array<Answer>();
    }
    this.selectedQuestion.answers.push(_answer);
  }

  deleteAnswer(answer : Answer)
  {
    if(this.selectedQuestion.answers.length<3) {
      return;
    }
    const index = this.selectedQuestion.answers.indexOf(answer, 0);
    console.log("answer: "+index);
    if (index > -1 && index<this.selectedQuestion.answers.length) {
      console.log("deleted answer: "+index);
      this.selectedQuestion.answers.splice(index, 1);
    }
  }

   selectedIsChoiceType():boolean {
    if(this.selectedQuestion.answers[0].questionType==0) {
      return true;
    } else {
      return false;
    }
  }

  copyAnswerWithoutSolution(a:Answer,userId:string)
  {
    //var answer=new Answer();
    var answer=a;
    answer.isCorrect=false;
    answer.trueOrFalse=false;
    if (answer.questionType!=0) {
      answer.textAnswer='';
    }
    answer.id=undefined;
    answer.userId=userId;
    return answer;
  }

  initUserAnswers(userid)
  {
    var userSolutions=new Array<Answer>();
    this.questions.forEach(q => {
      q.answers.forEach(a => {
        userSolutions.push(this.copyAnswerWithoutSolution(a,userid));
      });
    });
  }

  async postSolution() {
    var pdf:File;
    await this.generatePDF(false).then(value => {pdf=this.blobToFile(value,'solution.pdf');});
    var data = new FormData();
    data.append("data" , pdf,pdf.name);
    console.log(this.questions)
    data.append("model",JSON.stringify(this.questions));
    this.examService.postSolution(data,this.examid)
    .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate(['']);
                },
                error => {
                    this.error = error;
                });

  }

  public blobToFile = (theBlob: Blob, fileName:string): File => {
    var b: any = theBlob;
    //A Blob() is almost a File() - it's just missing the two properties below which we will add
    b.lastModifiedDate = new Date();
    b.name = fileName;

    //Cast to a File() type
    return <File>theBlob;
}

  public async generatePDF(save:boolean) : Promise<Blob> {
    var blobPDF:Blob;
    var html2canvas = html2canvasWrong as any as (element: HTMLElement, options?: Partial<html2canvasWrong.Options>) => Promise<HTMLCanvasElement>;
    var data = await document.getElementById('toPdf');
    await html2canvas(data).then(canvas => {
        // Few necessary setting options
        var imgWidth = 208;
        var pageHeight = 295;
        var imgHeight = canvas.height * imgWidth / canvas.width;
        var heightLeft = imgHeight;

        const contentDataURL = canvas.toDataURL('image/png')
        let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
        var position = 0;
        pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
        if(save) {
          pdf.save('MYPdf.pdf'); // Generated PDF
        }
        blobPDF =  new Blob([ pdf.output() ], { type : 'application/pdf'});
        console.log(blobPDF.size);
        //return blobPDF;

    });
    return blobPDF;
  }

  public navigateFinish()
  {
    this.toPdfVisible=true;
  }

  public navigateBack()
  {
    this.toPdfVisible=false;
  }

  testModel() {
    console.log('TEST: '+JSON.stringify(this.questions));
  }



}
