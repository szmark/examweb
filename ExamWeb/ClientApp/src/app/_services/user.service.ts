﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, Role } from '@/_models';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`${config.apiUrl}/api/auth/users`); /// /users
    }

    getById(id: string) {
        return this.http.get<User>(`${config.apiUrl}/api/auth/users/${id}`); // /users/${id}
    }

    getRoles() {
      return this.http.get<any[]>(`${config.apiUrl}/api/auth/roles`);
    }

    deleteUser(userId: string){
      return this.http.delete(`${config.apiUrl}/api/auth/user/${userId}`);
     }
     addUser(user: User){
       var email:string;
       email=user.username;
       console.log('TEST username: '+email);
       var id:string;
       id='';
       console.log('TEST id: '+id);
       var role:string;
       role=user.role.toString();
       console.log('TEST Role: '+role);
       var password='';
       console.log('TEST password: '+password);
       console.log(config.apiUrl);
       return this.http.post<any>(`${config.apiUrl}/api/auth/register`,
       {id ,email,role,password }) .pipe(map(user => {
         console.log(user);
         return user;
        }));
     }
     editUser(user: User){
      var email:string;
      email=user.username;
      var id:string;
      id=user.id;
      var role:string;
      role=user.role.toString();
      var password='';
      return this.http.put(`${config.apiUrl}/api/auth/user`,
      {id ,email,role,password }).pipe(map(user => {
        console.log(user);
        return user;
       }));
    }
}
