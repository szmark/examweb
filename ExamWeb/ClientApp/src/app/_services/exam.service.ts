import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Exam } from '@/_models/exam';
import { Subject } from '@/_models/subject';
import { UserGroup } from '@/_models/user-group';
import { map } from 'rxjs/operators';
import { Question } from '@/_models/question';
import { ExamStat } from '@/_models/exam-stat';
import { UserStat } from '@/_models/user-stat';
import { QuestionStat } from '@/_models/question-stat';

@Injectable({
  providedIn: 'root'
})
export class ExamService {

  constructor(private http: HttpClient) { }

  getExamsByTeacher(teacherid: string) {
    console.log('na mi az id: '+teacherid);
    return this.http.get<Exam[]>(`${config.apiUrl}/api/exam/self/exams/${teacherid}`);
  }
  getExamsByStudent(studentid: string) {
    return this.http.get<Exam[]>(`${config.apiUrl}/api/exam/access/exams/${studentid}`);
  }

  getExamById(id: string) {
    return this.http.get<Exam>(`${config.apiUrl}/api/exam/exams/${id}`);
  }
  getAllSubjects() {
    return this.http.get<Subject[]>(`${config.apiUrl}/api/exam/subjects`);
  }

  getSubjectById(id:number) {
    return this.http.get<Subject>(`${config.apiUrl}/api/exam/subjects/${id}`);
  }

  getAllUserGroups() { return this.http.get<UserGroup[]>(`${config.apiUrl}/api/exam/groups`); }

  updateExam(exam:Exam) {
    return this.http.put(`${config.apiUrl}/api/exam/updateExam`,
      exam).pipe(map(res => {
        console.log(res);
        return res;
       }));
  }

  addExam(exam : Exam)
  {
    return this.http.post(`${config.apiUrl}/api/exam/add`,
      exam).pipe(map(res => {
        console.log(res);
        return res;
       }));
  }

  postSolution(data:FormData,id:number) {
    return this.http.post(`${config.apiUrl}/api/exam/postAnswers/${id}`,
    data).pipe(map(res => {
        console.log(res);
        return res;
       }));
  }

  getExamsToStat(db:number, direction:boolean)
  {
    return this.http.get<ExamStat[]>(`${config.apiUrl}/api/exam/examsStat/${db}/${direction}`);
  }

  getUsersToStat(db:number, direction:boolean)
  {
    return this.http.get<UserStat[]>(`${config.apiUrl}/api/exam/usersStat/${db}/${direction}`);
  }

  getQuestionsToStat(db:number, direction:boolean)
  {
    return this.http.get<QuestionStat[]>(`${config.apiUrl}/api/exam/questionsStat/${db}/${direction}`);
  }

  setExamToRelease(id:number,newStatus:number) {
    return this.http.put(`${config.apiUrl}/api/exam/releaseExam/${id}`,
    newStatus).pipe(map(res => {
        console.log(res);
        return res;
       }));
    }

  setExamToClosed(id:number,newStatus:number) {
    return this.http.put(`${config.apiUrl}/api/exam/closeExam/${id}`,
    newStatus).pipe(map(res => {
        console.log(res);
        return res;
        }));
    }

    getUserResultByExam(id:number) {
      return this.http.get<UserStat[]>(`${config.apiUrl}/api/exam/examResult/${id}/students`);
    }

    getResultByUserAndExam(examid:number,userid:number) {
      return this.http.get<Question[]>(`${config.apiUrl}/api/exam/examResult/${examid}/students/${userid}`);
    }

    updateSolutionPts(data:FormData,id:number) {
      return this.http.post(`${config.apiUrl}/api/exam/correctExam/${id}`,
      data).pipe(map(res => {
          console.log(res);
          return res;
         }));
    }

}
