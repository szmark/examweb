import { User } from "@/_models";
import { Question } from "@/_models/question";
import { UserGroup } from "@/_models/user-group";
import { Subject } from "@/_models/subject";
import { ExamStatus } from "@/_models/exam-status";

export class Exam {
  id: number;
  title:string;
  description: string;
  user: User;
  //Subject még kell
  subject:Subject;
  toDate:number;
  fromDate:Date;
  status:ExamStatus;
  // question list
  questions:Question[];
  // user group list
  userGroup:UserGroup[];
  evaluateType:number;
}
