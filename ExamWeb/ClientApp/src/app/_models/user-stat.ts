export class UserStat {
  userID:string;
  name:string;
  avgPercent:number;
  ptsSum:number;
}
