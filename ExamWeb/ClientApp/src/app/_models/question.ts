import { QuestionType } from "@/_models/question-type.enum";
import { Answer } from "@/_models/answer";

export class Question {
  id:number;
  name:string;
  value?:number; // ez hogy hány pontot ér a kérdés
  //type:QuestionType;
  answers:Answer[];
  maxValue?:number;
  //válaszok listája

  /*constructor(id:number,name:string,value:number,type:QuestionType, answers:Answer[]) {
    this.id=id;
    this.name=name;
    this.value=value;
    this.type=type;
    this.answers=answers;
  }*/

  constructor() {}
}
