import { QuestionType } from "@/_models/question-type.enum";

export class Answer {
  id:number;
  textAnswer:string;
  trueOrFalse:boolean;
  answerA:string;
  answerB:string;
  isCorrect:boolean;
  userId:string;
  questionType:number;
}
