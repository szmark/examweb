﻿using ApplicationService.BusinessLogic;
using Data.Interfaces;
using Data.Models;
using Data.Repos;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Test
{
    [TestFixture]
    public class AnswerLogicTests
    {
        private Mock<IAnswerRepository> mockAnswers;
        //private Mock<QuestionRepository> mockQuestions;
        private AnswerLogic bl;
        private Random rand;
        List<Answer> Answers;

        [SetUp]
        public void SetUp()
        {
            this.rand = new Random();
            mockAnswers = new Mock<IAnswerRepository>();
             Answers = new List<Answer>()
            {
                new Answer() {  Id=1, TextAnswer="válasz1.1", IsCorrect=true, QuestionId=1,User=null},
                new Answer() {  Id=2, TextAnswer="válasz1.2", IsCorrect=false, QuestionId=1,User=null},
                new Answer() {  Id=3, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,User=null},
                new Answer() {  Id=4, TextAnswer="válasz1.1", IsCorrect=true, QuestionId=1,UserId="Dani",User=new ApplicationUser()},
                new Answer() {  Id=5, TextAnswer="válasz1.2", IsCorrect=true, QuestionId=1,UserId="Dani", User=new ApplicationUser()},
                new Answer() {  Id=6, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,UserId="Dani", User=new ApplicationUser()},
                new Answer() {  Id=7, TextAnswer="válasz1.1", IsCorrect=true, QuestionId=1,UserId="Zsolt", User=new ApplicationUser()},
                new Answer() {  Id=8, TextAnswer="válasz1.2", IsCorrect=false, QuestionId=1,UserId="Zsolt", User=new ApplicationUser()},
                new Answer() {  Id=9, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,UserId="Zsolt", User=new ApplicationUser()},
                new Answer() {  Id=10, TextAnswer="válasz2.1", IsCorrect=true, QuestionId=2,User=null},
                new Answer() {  Id=11, TextAnswer="válasz2.2", IsCorrect=false, QuestionId=2,User=null},
                new Answer() {  Id=12, TextAnswer="válasz2.1", IsCorrect=true, QuestionId=2,UserId="Zsolt", User=new ApplicationUser()},
                new Answer() {  Id=13, TextAnswer="válasz2.2", IsCorrect=true, QuestionId=2,UserId="Zsolt",User=new ApplicationUser()},
            };
            mockAnswers.Setup(x => x.GetAll()).ReturnsAsync(Answers);
            List<Question> Questions = new List<Question>()
            {
                new Question() { Id=1, Name ="kerdés1"},
                new Question() { Id=2, Name ="kerdés2"}
            };
           // this.mockQuestions.Setup(x => x.GetAll()).ReturnsAsync(Questions);
            this.bl = new AnswerLogic();
        }

        private static IEnumerable<object[]> SomeUserAnswerAndSolutionCases
        {
            get
            {
                yield return new object[]
                {
                    new List<Answer>()
                    {
                        new Answer() {  Id=4, TextAnswer="válasz1.1", IsCorrect=true, QuestionId=1,UserId="Dani"},
                        new Answer() {  Id=5, TextAnswer="válasz1.2", IsCorrect=true, QuestionId=1,UserId="Dani"},
                        new Answer() {  Id=6, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,UserId="Dani"}
                    },
                    new List<Answer>()
                    {
                        new Answer() {  Id=1, TextAnswer="válasz1.1", IsCorrect=true, QuestionId=1,User=null},
                        new Answer() {  Id=2, TextAnswer="válasz1.2", IsCorrect=false, QuestionId=1,User=null},
                        new Answer() {  Id=3, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,User=null}
                    },
                    0,
                    3,
                    0
                };

                yield return new object[]
                {
                    new List<Answer>()
                    {
                        new Answer() {  Id=4, TextAnswer="válasz1.1", IsCorrect=true, QuestionId=1,UserId="Dani"},
                        new Answer() {  Id=5, TextAnswer="válasz1.2", IsCorrect=true, QuestionId=1,UserId="Dani"},
                        new Answer() {  Id=6, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,UserId="Dani"}
                    },
                    new List<Answer>()
                    {
                        new Answer() {  Id=1, TextAnswer="válasz1.1", IsCorrect=true, QuestionId=1,User=null},
                        new Answer() {  Id=2, TextAnswer="válasz1.2", IsCorrect=false, QuestionId=1,User=null},
                        new Answer() {  Id=3, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,User=null}
                    },
                    1,
                    3,
                    -1.5
                };

                yield return new object[]
                {
                    new List<Answer>()
                    {
                        new Answer() {  Id=4, TextAnswer="válasz1.1", IsCorrect=true, QuestionId=1,UserId="Dani"},
                        new Answer() {  Id=5, TextAnswer="válasz1.2", IsCorrect=true, QuestionId=1,UserId="Dani"},
                        new Answer() {  Id=6, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,UserId="Dani"}
                    },
                    new List<Answer>()
                    {
                        new Answer() {  Id=1, TextAnswer="válasz1.1", IsCorrect=true, QuestionId=1,User=null},
                        new Answer() {  Id=2, TextAnswer="válasz1.2", IsCorrect=false, QuestionId=1,User=null},
                        new Answer() {  Id=3, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,User=null}
                    },
                    2,
                    3,
                    2
                };

                yield return new object[]
                {
                    new List<Answer>()
                    {
                        new Answer() {  Id=7, TextAnswer="válasz1.1", IsCorrect=true, QuestionId=1,UserId="Zsolt"},
                        new Answer() {  Id=8, TextAnswer="válasz1.2", IsCorrect=false, QuestionId=1,UserId="Zsolt"},
                        new Answer() {  Id=9, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,UserId="Zsolt"}
                    },
                    new List<Answer>()
                    {
                        new Answer() {  Id=1, TextAnswer="válasz1.1", IsCorrect=true, QuestionId=1,User=null},
                        new Answer() {  Id=2, TextAnswer="válasz1.2", IsCorrect=false, QuestionId=1,User=null},
                        new Answer() {  Id=3, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,User=null}
                    },
                    0,
                    3,
                    3
                };

                yield return new object[]
                {
                    new List<Answer>()
                    {
                        new Answer() {  Id=7, TextAnswer="válasz1.1", IsCorrect=true, QuestionId=1,UserId="Zsolt"},
                        new Answer() {  Id=8, TextAnswer="válasz1.2", IsCorrect=false, QuestionId=1,UserId="Zsolt"},
                        new Answer() {  Id=9, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,UserId="Zsolt"}
                    },
                    new List<Answer>()
                    {
                        new Answer() {  Id=1, TextAnswer="válasz1.1", IsCorrect=true, QuestionId=1,User=null},
                        new Answer() {  Id=2, TextAnswer="válasz1.2", IsCorrect=false, QuestionId=1,User=null},
                        new Answer() {  Id=3, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,User=null}
                    },
                    1,
                    3,
                    3
                };
                yield return new object[]
                {
                    new List<Answer>()
                    {
                        new Answer() {  Id=7, TextAnswer="válasz1.1", IsCorrect=true, QuestionId=1,UserId="Zsolt"},
                        new Answer() {  Id=8, TextAnswer="válasz1.2", IsCorrect=false, QuestionId=1,UserId="Zsolt"},
                        new Answer() {  Id=9, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,UserId="Zsolt"}
                    },
                    new List<Answer>()
                    {
                        new Answer() {  Id=1, TextAnswer="válasz1.1", IsCorrect=true, QuestionId=1,User=null},
                        new Answer() {  Id=2, TextAnswer="válasz1.2", IsCorrect=false, QuestionId=1,User=null},
                        new Answer() {  Id=3, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,User=null}
                    },
                    2,
                    3,
                    3
                };

                yield return new object[]
                {
                    new List<Answer>()
                    {
                        new Answer() {  Id=7, TextAnswer="válasz1.1", IsCorrect=false, QuestionId=1,UserId="Zsolt"},
                        new Answer() {  Id=8, TextAnswer="válasz1.2", IsCorrect=false, QuestionId=1,UserId="Zsolt"},
                        new Answer() {  Id=9, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,UserId="Zsolt"}
                    },
                    new List<Answer>()
                    {
                        new Answer() {  Id=1, TextAnswer="válasz1.1", IsCorrect=true, QuestionId=1,User=null},
                        new Answer() {  Id=2, TextAnswer="válasz1.2", IsCorrect=false, QuestionId=1,User=null},
                        new Answer() {  Id=3, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,User=null}
                    },
                    0,
                    3,
                    0
                };

                yield return new object[]
                {
                    new List<Answer>()
                    {
                        new Answer() {  Id=7, TextAnswer="válasz1.1", IsCorrect=false, QuestionId=1,UserId="Zsolt"},
                        new Answer() {  Id=8, TextAnswer="válasz1.2", IsCorrect=false, QuestionId=1,UserId="Zsolt"},
                        new Answer() {  Id=9, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,UserId="Zsolt"}
                    },
                    new List<Answer>()
                    {
                        new Answer() {  Id=1, TextAnswer="válasz1.1", IsCorrect=true, QuestionId=1,User=null},
                        new Answer() {  Id=2, TextAnswer="válasz1.2", IsCorrect=false, QuestionId=1,User=null},
                        new Answer() {  Id=3, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,User=null}
                    },
                    1,
                    3,
                    0
                };

                yield return new object[]
                {
                    new List<Answer>()
                    {
                        new Answer() {  Id=7, TextAnswer="válasz1.1", IsCorrect=false, QuestionId=1,UserId="Zsolt"},
                        new Answer() {  Id=8, TextAnswer="válasz1.2", IsCorrect=false, QuestionId=1,UserId="Zsolt"},
                        new Answer() {  Id=9, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,UserId="Zsolt"}
                    },
                    new List<Answer>()
                    {
                        new Answer() {  Id=1, TextAnswer="válasz1.1", IsCorrect=true, QuestionId=1,User=null},
                        new Answer() {  Id=2, TextAnswer="válasz1.2", IsCorrect=false, QuestionId=1,User=null},
                        new Answer() {  Id=3, TextAnswer="válasz1.3", IsCorrect=false, QuestionId=1,User=null}
                    },
                    2,
                    3,
                    2
                };

            }
        }

        [TestCaseSource("SomeUserAnswerAndSolutionCases")]
        public void CalcPTSToQuestionTest(IEnumerable<Answer> userAnswers, IEnumerable<Answer> Solutions, EvaluationType type, double maxpont, double expextedValue)
        {
            var res = this.bl.CalcPTSToQuestion(userAnswers, Solutions, type, maxpont);
            Assert.That(res, Is.EqualTo(expextedValue));
        }

        [TestCase(1,"Dani",3)]
        [TestCase(1, "Zsolt",3)]
        [TestCase(1, "Zsol", 0)]
        public async Task GetUserAnswersForQuestionTest(int questionId, string UserId,int expectedCount)
        {
            //var answers = mockAnswers.Object.GetAll();
            //answers.Wait();
            var res =await   this.bl.FindUserAnswersForQuestion(mockAnswers.Object ,questionId, UserId);
            List<Answer> result = (List<Answer>) res;
            Assert.That(result.Count, Is.EqualTo(expectedCount));

        }

        [TestCase(1, 3)]        
        [TestCase(2, 2)]
        public async Task GetSolutionsForQuestionTest(int questionId,int expectedCount)
        {           
            var res =  await this.bl.FindSolutionsForQuestion(mockAnswers.Object, questionId);           
            List<Answer> result = (List < Answer >) res;
            Assert.That(result.Count, Is.EqualTo(expectedCount));
        }

        [TestCase(13)]
        public async Task  AllAnswerCountTest(int expectedCount)
        {
            var res = await this.bl.GetAllAnswer(mockAnswers.Object);
            List<Answer> result = (List<Answer>)res;
            Assert.That(result.Count, Is.EqualTo(expectedCount));
        }
    }
}
