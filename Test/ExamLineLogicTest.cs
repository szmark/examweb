﻿using ApplicationService.BusinessLogic;
using ApplicationService.ViewModels;
using Data;
using Data.Interfaces;
using Data.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    public class ExamLineLogicTest
    {
        private Mock<IExamLineRepository> mockExamLines;
        //private Mock<QuestionRepository> mockQuestions;
        private ExamLineLogic bl;
        private Random rand;
        List<ExamLine> ExamLines;
        List<Exam> Exams;
        List<ApplicationUser> users;

        [SetUp]
        public void SetUp()
        {
            this.rand = new Random();
            mockExamLines = new Mock<IExamLineRepository>();
            ExamLines = ExamLineFactory();           
            mockExamLines.Setup(x => x.GetAll()).ReturnsAsync(ExamLines);
            this.bl = new ExamLineLogic();
        }

        [TestCase(1,1, 6)]
        [TestCase(2,1, 10)]
        [TestCase(3,1, 4)]
        [TestCase(4,2, 6)]
        public void GetAchiveAbleValueTest(int questionid,int examId, double expectedValue)
        {
            var res = this.bl.GetAchiveAbleValue(ExamLines,questionid,examId);
            Assert.That(res, Is.EqualTo(expectedValue));
        }

        [TestCase(0, 0.7,0.76)]
        [TestCase(1, 0.7,0.76)]
        [TestCase(2, 0.6,0.7)]
        [TestCase(3, 0.5,0.6)]
        public async Task TopXMostEasyQuestionTestPercentValue(int i, double expectedFrom,double expectedTo)
        {
            var res = await this.bl.TopXMostEasyQuestion(5, mockExamLines.Object);
            List<QuestionStatViewModel> result = (List<QuestionStatViewModel>)res;
            Assert.That(result[i].AvgPercent, Is.GreaterThan(expectedFrom));
            Assert.That(result[i].AvgPercent, Is.LessThan(expectedTo));
        }

        [TestCase(3, 0.7, 0.76)]
        [TestCase(2, 0.7, 0.76)]
        [TestCase(1, 0.6, 0.7)]
        [TestCase(0, 0.5, 0.6)]
        public async Task TopXLeastEasyQuestionTestPercentValue(int i, double expectedFrom, double expectedTo)
        {
            var res = await this.bl.TopXLeastEasyQuestion(5, mockExamLines.Object);
            List<QuestionStatViewModel> result = (List<QuestionStatViewModel>)res;
            Assert.That(result[i].AvgPercent, Is.GreaterThan(expectedFrom));
            Assert.That(result[i].AvgPercent, Is.LessThan(expectedTo));
        }

        [TestCase(0,"Zsolt")]
        [TestCase(1, "Géza")]
        public async Task TopXMostSuccesfulUsers(int i,string ExpectedName)
        {
            var res = await bl.TopXMostSuccesfulUsers(2, mockExamLines.Object, users);
            List<UserStatViewModel> result = (List<UserStatViewModel>)res;
            Assert.That(result[i].Name, Is.EqualTo(ExpectedName));
        }

        [TestCase(0, "Dani")]
        [TestCase(1, "Géza")]
        public async Task TopXLeastSuccesfulUsers(int i, string ExpectedName)
        {
            var res = await bl.TopXLeastSuccesfulUsers(2, mockExamLines.Object, users);
            List<UserStatViewModel> result = (List<UserStatViewModel>)res;
            Assert.That(result[i].Name, Is.EqualTo(ExpectedName));
        }

        [TestCase(0, "Matematika Vizsga")]
        [TestCase(1, "Informatika Vizsga")]
        [TestCase(2, "Történelem Vizsga")]
        public async Task TopXMostDifficultExamTest(int i, string ExpectedTitle)
        {
            var res = await bl.TopXMostDifficultExam(3, mockExamLines.Object, Exams);
            List<ExamStatViewModel> result = (List<ExamStatViewModel>)res;
            Assert.That(result[i].Title, Is.EqualTo(ExpectedTitle));
        }

        [TestCase(0, "Történelem Vizsga")]
        [TestCase(1, "Informatika Vizsga")]
        [TestCase(2, "Matematika Vizsga")]
        public async Task TopLeastDifficultExamTest(int i, string ExpectedTitle)
        {
            var res = await bl.TopXLeastDifficultExam(3, mockExamLines.Object, Exams);
            List<ExamStatViewModel> result = (List<ExamStatViewModel>)res;
            Assert.That(result[i].Title, Is.EqualTo(ExpectedTitle));
        }


        public List<ExamLine> ExamLineFactory()
        {
            users = new List<ApplicationUser>()
            {
                new ApplicationUser() {Id="1", UserName="Dani"},
                new ApplicationUser() {Id="2", UserName="Géza"},
                new ApplicationUser() {Id="3", UserName="Zsolt"},
            };

            Exams = new List<Exam>()
            {
                new Exam() {Id=1, Title="Matematika Vizsga" },
                new Exam() {Id=2, Title="Informatika Vizsga" },
                new Exam() {Id=3, Title="Történelem Vizsga" },
            };

            List<ExamLine > ExamLines = new List<ExamLine>()
            {
                new ExamLine() {
                    Exam =Exams[0], ExamId=1,
                    LineId=1,
                    Question=new Question() { }, QuestionId=1, QuestionName="Kérdés1",
                    User=users[0] , UserId="1",
                    Value=3
                },
                new ExamLine() {
                    Exam =Exams[0], ExamId=1,
                    LineId=1,
                    Question=new Question() { }, QuestionId=1, QuestionName="Kérdés1",
                    //User=new ApplicationUser() { }, UserId="1",
                    Value=6
                },
                new ExamLine() {
                    Exam =Exams[0], ExamId=1,
                    LineId=1,
                    Question=new Question() { }, QuestionId=1, QuestionName="Kérdés1",
                    User=users[1], UserId="2",
                    Value=6
                },
                new ExamLine() {
                    Exam =Exams[0], ExamId=1,
                    LineId=1,
                    Question=new Question() { }, QuestionId=2, QuestionName="Kérdés2",
                    //User=new ApplicationUser() { }, UserId="1",
                    Value=10
                },
                new ExamLine() {
                    Exam =Exams[0], ExamId=1,
                    LineId=1,
                    Question=new Question() { }, QuestionId=2, QuestionName="Kérdés2",
                    User=users[0], UserId="1",
                    Value=2
                },
                new ExamLine() {
                    Exam =Exams[0], ExamId=1,
                    LineId=1,
                    Question=new Question() { }, QuestionId=2, QuestionName="Kérdés2",
                    User=users[1], UserId="2",
                    Value=5
                },
                new ExamLine() {
                    Exam =Exams[2], ExamId=3,
                    LineId=1,
                    Question=new Question() { }, QuestionId=2, QuestionName="Kérdés2",
                    //User=new ApplicationUser() { }, UserId="1",
                    Value=3
                },
                new ExamLine() {
                    Exam =Exams[2], ExamId=3,
                    LineId=1,
                    Question=new Question() { }, QuestionId=2, QuestionName="Kérdés2",
                    User=users[2], UserId="3",
                    Value=3
                },
                new ExamLine() {
                    Exam =Exams[0], ExamId=1,
                    LineId=1,
                    Question=new Question() { }, QuestionId=3, QuestionName="Kérdés3",
                    //User=new ApplicationUser() { }, UserId="1",
                    Value=4
                },
                new ExamLine() {
                    Exam =Exams[0], ExamId=1,
                    LineId=1,
                    Question=new Question() { }, QuestionId=3, QuestionName="Kérdés3",
                    User=users[0], UserId="1",
                    Value=2
                },
                new ExamLine() {
                    Exam =Exams[0], ExamId=1,
                    LineId=1,
                    Question=new Question() { }, QuestionId=3, QuestionName="Kérdés3",
                    User=users[1], UserId="2",
                    Value=4
                },
                new ExamLine() {
                    Exam =Exams[1], ExamId=2,
                    LineId=1,
                    Question=new Question() { }, QuestionId=4, QuestionName="Kérdés4",
                    //User=new ApplicationUser() { }, UserId="1",
                    Value=6
                },
                new ExamLine() {
                    Exam =Exams[1], ExamId=2,
                    LineId=1,
                    Question=new Question() { }, QuestionId=4, QuestionName="Kérdés4",
                    User=users[0], UserId="1",
                    Value=4
                }
            };
            return ExamLines;
        }
    }
}
